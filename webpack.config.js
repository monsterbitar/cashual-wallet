//require("@babel/register");

module.exports = 
{
	// Add your application's scripts below
	entry: [ './www/js/source.js' ],
	output: 
	{
		filename: './www/js/application.js'
	},
	module:
	{
		rules:
		[
			{
				// Only run `.js` files through Babel
				test: /\.js$/,

				// Do not parse node modules.
				include:
				[
					/www/,
					/node_modules/
				],

				// Load babel to translate from ES6/7 down to ES5.
				use: [ 'babel-loader' ],
			},
		]
	},
	node:
	{
		fs: 'empty',
		net: 'empty',
		tls: 'empty',
	},
}
