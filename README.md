# Cashual Wallet

A basic but highly userfriendly Bitcoin Cash wallet focused on Cash Account integration.

# Dependencies

# Build setup instructions

git clone https://gitlab.com/monsterbitar/cashual-wallet.git

cd cashual-wallet/

npm install

cordova plugin add src/plugins/cashual_wallet/

cordova platform add android

npx webpack --mode none --entry ./www/js/source.js --output ./www/js/application.js

cordova run android