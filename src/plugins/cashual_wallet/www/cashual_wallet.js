var exec = require('cordova/exec');

exports.copyToClipboard = function(arg0, success, error)
{
	exec(success, error, 'cashual_wallet', 'copyToClipboard', [arg0]);
};

exports.getUserLocale = function(arg0, success, error)
{
	exec(success, error, 'cashual_wallet', 'getUserLocale', [arg0]);
};
