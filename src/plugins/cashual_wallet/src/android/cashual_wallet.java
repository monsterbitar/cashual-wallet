package org.monsterbitar.cashual_wallet;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.text.ClipboardManager;

import java.util.Locale;
import java.util.Currency;

/**
 * This is a set of helper functions for the cashual wallet.
 */
public class cashual_wallet extends CordovaPlugin
{
	private ClipboardManager clipboard;

	@Override
	public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException
	{
		if(clipboard == null)
		{
			clipboard = (ClipboardManager) cordova.getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
		}
	
		if(action.equals("copyToClipboard"))
		{
			// Copy the text to the default system clipboard.
			clipboard.setText(args.getString(0));

			// Return true to indicate successful copy to clipboard.
			callbackContext.success();

			// Return to stop exectution.
			return true;
		}

		if(action.equals("getUserLocale"))
		{
			Locale locale = Locale.getDefault();
			Currency currency = Currency.getInstance(locale);

			callbackContext.success(String.format("{ \"languageCode\": \"%s\", \"languageName\": \"%s\", \"currencyCode\": \"%s\", \"currencyName\": \"%s\" }", locale.getLanguage(), locale.getDisplayLanguage(), currency.getCurrencyCode(), currency.getDisplayName()));
		}

		return false;
	}
}
