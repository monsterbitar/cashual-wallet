// Add polyfills to support older devices.
require("@babel/polyfill");

// import logging functionality.
const log = require("./logging.js");

// Enable the logs.
log.debug.enabled = true;
log.notice.enabled = true;
log.warning.enabled = true;
log.error.enabled = true;
log.fatal.enabled = true;

// Mark the start of dependency loading.
log.notice('Loading dependencies.');
log.debug('');

// Enable helper tool to promisify callback based libraries.
const promisify = require('util-promisifyall');

// Mark this dependency.
log.debug('- promisify');

// Enable use of moment to handle time.
const moment = require('moment');

// Mark this dependency.
log.debug('- moment.js');

// Load support library to generate QR codes.
const qrcode = require('pure-svg-code/qrcode');

// Mark this dependency.
log.debug('- qrcode');

// Load the bitcore wallet client library.
const bitcoreWallet = require('bitcore-wallet-client');

// Mark this dependency.
log.debug('- bitcore wallet');

// Import configuration settings.
const translations = require("./translations.js");
const templates = require("./templates.js");

// Mark this dependency.
log.debug('- app data');

// Create an empty function to use as non-action callbacks.
const passthrough = function(error) { console.log('PASSTHROUGH: '); console.log(error); }
const requestGet = require("./util/requestGet.js");
const requestPostJSON = require("./util/requestPostJSON.js");

// Mark this dependency.
log.debug('- xmlhttp wrappers');

// Load support for cashaccounts
const cashaccount = require("./cashaccount.js");

// Mark this dependency.
log.debug('- cashaccount');

// TODO: Load support for cashid.
const cashid = require("./cashid.js");

// Mark this dependency.
log.debug('- cashid');

// Mark the end of the dependecy loading.
log.notice('Loaded dependencies.');

const throwDebug = function(message, error)
{
	log.debug(message);
	log.debug(error);
	throw(message + ' - ' + JSON.stringify(error));
}

const stackTrace = function()
{
	// Generate an empty error.
	let err = new Error();
	
	// Return the stack trace.
	return err.stack;
}

// Define what node we will use for our blockchain data provider.
const NODE_URL = 'https://insight.imaginary.cash/';
const NODE_BWS = NODE_URL + 'bws/api/';
const NODE_NET = NODE_URL + 'api/BCH/mainnet/';


const VIBRATE =
{
	// Single quick tap.
	NOTIFY: [125],
	
	// Knock, Knock, Knock
	ATTENTION: [125, 100, 125, 100, 125],
	
	// Short and long, order depending on outcome.
	SUCCESS: [200, 66, 500],
	ERROR: [500, 66, 200],
}

class CashualWallet
{
	async importWallet()
	{
		try
		{
		}
		catch(error)
		{
			throwDebug('Failed to import wallet', error);
		}
	}

	async restoreAccount(identifier)
	{
		try
		{
			// Match the name to a parser to get the account parts.
			let accountParts = identifier.match(cashaccount.identifierRegexp);
			
			// Name the parts for convenience.
			let name = accountParts[1];
			let number = accountParts[2];
			let hash = accountParts[4];

			// TODO: Properly handle collisions
			// Look up the account on a public API.
			let lookupAccount = JSON.parse(await requestGet('https://api.cashaccount.info/lookup/' + number + '/' + name));
			
			// Look up the block information from a public API
			let lookupBlock = JSON.parse(await requestGet(NODE_NET + '/block/' + (cashaccount.blockModifier + parseInt(number))));
			
			// Parse the registration transaction to get the account details.
			let finalAccount = await cashaccount.parseRegistration(Buffer.from(lookupAccount.results[0].transaction, 'hex'), Buffer.from(lookupBlock.hash, 'hex'), lookupAccount.results[0].inclusion_proof);

			// Go over each payload..
			for(let index in finalAccount.payloads)
			{
				// If this is a KeyHash or ScriptHash...
				if(finalAccount.payloads[index].type == 1 || finalAccount.payloads[index].type == 2)
				{
					// .. and it matches our wallets address ..
					if(finalAccount.payloads[index].address == ('bitcoincash:' + window.app.wallet.address))
					{
						// Initialize an the account structure to hold the account information.
						window.app.wallet.account = finalAccount;

						// Update the account number.
						window.app.wallet.account.number = number;

						// Get the registration transactions TXID and mark it as already processed.
						window.app.wallet.registrationTxId = bitcoreWallet.BitcoreCash.Transaction(Buffer.from(lookupAccount.results[0].transaction, 'hex')).hash;
						window.app.wallet.history.transactions[window.app.wallet.registrationTxId] = true;
					}
				}
			}
			
			// If we failed to find a valid account
			if(!window.app.wallet.account)
			{
				throwDebug('Failed to restore account: payment data does not match wallet.', null);
			}
		}
		catch(error)
		{
			throwDebug('Failed to restore account', error);
		}
	}
	
	async registerAccount(name, address)
	{
		try
		{
			// Request a cash account registration from the public API.
			let registrationData = JSON.parse(await requestPostJSON('https://api.cashaccount.info/register/', { name: name, payments: [ address ] }));
			
			// Store the registration transaction and id.
			window.app.wallet.registrationTxId = registrationData.txid;
			window.app.wallet.registrationTxHex = registrationData.hex;
			
			// Request a notification upon completed transaction.
			await window.app.walletManager.txConfirmationSubscribeAsync({ txid: window.app.wallet.registrationTxId });
			
			// Create a history entry, with a 1ms shift to ensure that it happens after the new wallet entry.
			window.app.wallet.history.pending.push({ timestamp: moment().valueOf() + 1, transaction: window.app.wallet.registrationTxId, type: 'system', icon: '--user', text: 'Registering Cash Account', value: null, currency: null, translation: 'views.home.accountPending' });
			
			// Initialize an empty structure to hold the account information.
			window.app.wallet.account = await cashaccount.parseRegistration(Buffer.from(window.app.wallet.registrationTxHex, 'hex'), null);
		}
		catch(error)
		{
			throwDebug('Failed to register account', error);
		}
	}
	
	async createWallet(name, privateKey = null, cashAccount = null, updateHistory = true)
	{
		try
		{
			// Create a wallet structure from the default templates.
			window.app.wallet = templates.wallet;

			// Store the creation date of the wallet.
			window.app.wallet.birth = moment().valueOf();

			// Update last verified timestamp.
			window.app.wallet.lastVerified = moment.valueOf();

			// If a private key was supplied..
			if(privateKey)
			{
				// Create a history entry.
				window.app.wallet.history.entries.push({ timestamp: moment().valueOf(), transaction: 1, type: 'system', icon: '--wallet', text: 'Restored from backup', value: null, currency: null, translation: 'views.home.backupRestored' });

				try
				{
					await window.app.walletManager.importFromExtendedPrivateKeyAsync(privateKey, { coin: 'bch' })
				}
				catch(error)
				{
					// Restore an old 1-of-1 HD wallet using above seed.
					await window.app.walletManager.createWalletAsync(name, name, 1, 1, { coin: 'bch', network: 'livenet', singleAddress: true });
				}
			}
			else
			{
				//TODO: Translate this.
				window.plugins.toast.showLongCenter('Creating wallet');

				// Create a history entry.
				window.app.wallet.history.entries.push({ timestamp: moment().valueOf(), transaction: 1, type: 'system', icon: '--wallet', text: 'New wallet', value: null, currency: null, translation: 'views.home.walletCreated' });

				// Create a new 1-of-1 HD wallet.
				await window.app.walletManager.createWalletAsync(name, name, 1, 1, { coin: 'bch', network: 'livenet', singleAddress: true });
			}

			// Check if the wallet is functional..
			if(window.app.walletManager.isComplete())
			{
				// Store the wallet secret in the structure.
				window.app.wallet.secret = window.app.walletManager.export();
			}

			try
			{
				// Create a new address.
				let addressData = await window.app.walletManager.createAddressAsync({});

				// Locally store the CashAddr version of the address.
				window.app.wallet.address = bitcoreWallet.BitcoreCash.Address(addressData.address).toCashAddress(true);
			}
			catch(error)
			{
				throwDebug('Failed to create a wallet address: ' + error);
			}

			// If an account was supplied..
			if(cashAccount)
			{
				// Use an existing cash account if it matches the same payment data.
				await window.app.restoreAccount(cashAccount);
			}
			else
			{
				// Request an account registration.
				await window.app.registerAccount(name, window.app.wallet.address);
			}

			// Store the changes to local storage.
			localStorage.wallet = JSON.stringify(window.app.wallet);

			// Setup up the wallet presentation.
			await window.app.setupWallet();

			if(updateHistory)
			{
				// Update the presentation of the transaction history.
				await window.app.updateTransactionHistory(true);
			}
		}
		catch(error)
		{
			throwDebug('Failed to create wallet', error);
		}
	}

	async updateAccountPresentation()
	{
		try
		{
			// If we have a wallet, and the wallet has some account information...
			if(typeof window.app.wallet !== 'undefined' && typeof window.app.wallet.account == 'object')
			{
				// Update the account name.
				document.getElementById('drawer_account_name').innerHTML = window.app.wallet.account.name;
				document.getElementById('receive_account_name').innerHTML = window.app.wallet.account.name;
				
				// TODO: properly evaluate collisions.
				// Update the account number (and hash once we've properly detected collisions)
				document.getElementById('drawer_account_number_and_hash').innerHTML = '#' + (window.app.wallet.account.number ? window.app.wallet.account.number : '?');
				document.getElementById('receive_account_number_and_hash').innerHTML = '#' + (window.app.wallet.account.number ? window.app.wallet.account.number : '?');

				// Update the account icon.
				document.getElementById('drawer_account_icon').innerHTML = (window.app.wallet.account.emoji ? window.app.wallet.account.emoji : '?');
				document.getElementById('receive_account_icon').innerHTML = (window.app.wallet.account.emoji ? window.app.wallet.account.emoji : '?');
			}
		}
		catch(error)
		{
			throwDebug('Failed to update account presentation', error);
		}
	}

	async handleStuckProposals()
	{
		try
		{
			// Ask for a list of pending proposals.
			let proposals = await window.app.walletManager.getTxProposalsAsync({});
			
			// For each proposal..
			for(let index in proposals)
			{
				try
				{
					// Try to reject the proposal.
					await window.app.walletManager.rejectTxProposalAsync(proposals[index], 'Rejecting unwanted proposal');
				}
				catch(error)
				{
					try
					{
						// Failing to reject the proposal, try to settle it on the chain.
						await window.app.walletManager.broadcastTxProposalAsync(proposals[index]);
					}
					catch(error)
					{
						try
						{
							// Worst case, attempt to just remove it and hope for the best.
							await window.app.walletManager.removeTxProposalAsync(proposals[index]);
						}
						catch(error)
						{
							// Failed to remove, giving up..
							alert('There is a pending proposal that we were unable to deal with. Details: ' + JSON.stringify(error));
							alert(JSON.stringify(proposals[index]));
						}
					}
				}
			}
		}
		catch(error)
		{
			throwDebug('Failed to reject/remove/broadcast stuck proposals', error);
		}
	}

	async setupWallet()
	{
		try
		{
			// If no wallet exist in storage yet..
			if(typeof window.app.wallet !== 'undefined' && typeof window.app.wallet.secret !== 'undefined')
			{
				// Setup the wallet.
				window.app.walletManager.import(window.app.wallet.secret);
			}

			// If we have a working wallet..
			if(window.app.walletManager.isComplete())
			{
				// Update the receive screen with the current address.
				window.app.updateAddress();

				// Update the cash account presentation on the receive view and in the drawer.
				window.app.updateAccountPresentation();

				// Open it wallet so we can work with it.
				await window.app.walletManager.openWalletAsync();
				
				// Periodically update the transction history.
				setInterval(window.app.updateTransactionHistory, 15000);

				// Clear out all stuck proposals.
				
				// BACKGROUND:
				// Sending funds is a multistep process, and in rare cases fails.
				// When this happens, it is better that we automatically resolve it, 
				// than it is to let user experience degrade even further.
				await window.app.handleStuckProposals();
				
				// Wait for the updating of currency data if needed.
				await window.app.updatingCurrencyRates;
				
				// Get and update the visible wallet balance.
				await window.app.updateWalletBalance();

				// Get wallet recent notifications.
			}
		}
		catch(error)
		{
			throwDebug('Failed to request a list of pending proposals', error);
		}
	}

	// Update the receive screen with the current address.
	async updateAddress(amount = false)
	{
		try
		{
			// If this wallet has an address..
			if(typeof window.app.wallet.address != 'undefined')
			{
				if(amount)
				{
					// Update the receive screen to represent the wallet.
					document.getElementById('receive_address').innerHTML = window.app.wallet.address;
					document.getElementById('receive_qrcode').innerHTML = qrcode({ content: "bitcoincash:" + window.app.wallet.address + '?amount=' + amount, background: 'rgba(0, 0, 0, 0)' });
				}
				else
				{
					// Update the receive screen to represent the wallet.
					document.getElementById('receive_address').innerHTML = window.app.wallet.address;
					document.getElementById('receive_qrcode').innerHTML = qrcode({ content: "bitcoincash:" + window.app.wallet.address, background: 'rgba(0, 0, 0, 0)' });
				}
			}
		}
		catch(error)
		{
			throwDebug('Failed to update wallet address', error);
		}
	}

	async updateHistoryContactList()
	{
		try
		{
			// Initialize an empty set of nodes
			let contactNodes = [];

			//
			let template = document.querySelector('#historyContactTemplate');

			if(typeof window.app.wallet !== 'undefined')
			{
				// Create all translation entries.
				for(let index in window.app.wallet.history.pending)
				{
					if(window.app.wallet.history.pending[index] !== null)
					{
						// Check if this entry is an outgoing transaction with an icon-text..
						if(window.app.wallet.history.pending[index].type == 'sent' && window.app.wallet.history.pending[index].iconText)
						{
							//
							let currentNode = document.importNode(template.content, true);
							let currentName = window.app.wallet.history.pending[index].text;
							let currentIcon = window.app.wallet.history.pending[index].iconText;

							//
							currentNode.querySelector('.title').innerHTML = currentName;
							currentNode.querySelector('.icon').innerHTML = currentIcon;
							currentNode.querySelector('.row').setAttribute('data-contact', currentName);

							// If this name has not yet been added..
							if(contactNodes.findIndex((obj => obj.name.toLowerCase() == currentName.toLowerCase())) == -1)
							{
								//
								contactNodes.push({ name: currentName, node: currentNode });
							}
						}
					}
				}

				// Create all translation entries.
				for(let index in window.app.wallet.history.entries)
				{
					if(window.app.wallet.history.entries[index] !== null)
					{
						// Check if this entry is an outgoing transaction with an icon-text..
						if(typeof window.app.wallet.history.entries[index].type !== 'undefined' && window.app.wallet.history.entries[index].type == 'sent')
						{
							if(typeof window.app.wallet.history.entries[index].iconText !== 'undefined' && window.app.wallet.history.entries[index].iconText)
							{
								//
								let currentNode = document.importNode(template.content, true);
								let currentName = window.app.wallet.history.entries[index].text;
								let currentIcon = window.app.wallet.history.entries[index].iconText;

								//
								currentNode.querySelector('.title').innerHTML = currentName;
								currentNode.querySelector('.icon').innerHTML = currentIcon;
								currentNode.querySelector('.row').setAttribute('data-contact', currentName);

								// If this name has not yet been added..
								if(contactNodes.findIndex((obj => obj.name.toLowerCase() == currentName.toLowerCase())) == -1)
								{
									//
									contactNodes.push({ name: currentName, node: currentNode });
								}
							}
						}
					}
				}
			}

			// Sort contacts by name.
			contactNodes.sort(window.app.compareObjectNamesInsensitive);

			if(contactNodes.length == 0)
			{
				if(!document.getElementById('historyContactList').hasAttribute('no-contacts'))
				{
					document.getElementById('historyContactList').setAttribute('no-contacts', 1);
					document.getElementById('historyContactList').innerHTML += "<i class='row muted' style='text-align: center'>You have no payment history.</i>";
				}
			}
			else
			{
				if(document.getElementById('historyContactList').hasAttribute('no-contacts'))
				{
					document.getElementById('historyContactList').removeAttribute('no-contacts', 1);
					
					let childNode = document.getElementById('historyContactList').getElementsByTagName('i')[0];
					childNode.parentNode.removeChild(childNode);
				}
				
				// Clear out all previous contacts from the list.
				let contactEntries = document.querySelectorAll('#historyContactList > label');

				if(contactEntries !== null)
				{
					for(let index = contactEntries.length - 1; index >= 0; index--)
					{
						contactEntries[index].parentNode.removeChild(contactEntries[index]);
					}
				}

				// For each contact entry..
				for(let index in contactNodes)
				{
					// Add it to the translation list.
					document.getElementById('historyContactList').appendChild(contactNodes[index].node);
				}
			}
		}
		catch(error)
		{
			throwDebug('Failed to update history contact list', error);
		}
	}

	async updateTransactionHistory(newTransactions = false, newValue = false)
	{
		try
		{
			// Set the loading indicator to working state.
			document.getElementById('status_history').className = 'loading row'
			document.getElementById('status_history').childNodes[0].className = 'icon fas fa-sync-alt'

			// TODO: ....
			let data = await window.app.walletManager.getTxHistoryAsync({ limit: 50, includeExtendedInfo: true });

			// Wait for updated currency data if necessary.
			await window.app.updatingCurrencyRates;

			// If we have a pending account registration transactions..
			if(!window.app.wallet.history.transactions[window.app.wallet.registrationTxId])
			{
				// Request information on the transaction from a public API.
				let accountRegistration = JSON.parse(await requestGet(NODE_NET + 'tx/' + window.app.wallet.registrationTxId));

				// If the account registration is confirmed..
				// NOTE: The check against the blockHeight is due to a bug in BWS reporting a negative confirmation count.
				if(accountRegistration.confirmations > 0 || accountRegistration.blockHeight > 0)
				{
					// Update the empty structure with new account information.
					window.app.wallet.account = await cashaccount.parseRegistration(Buffer.from(window.app.wallet.registrationTxHex, 'hex'), Buffer.from(accountRegistration.blockHash, 'hex'));

					// Update the account number.
					window.app.wallet.account.number = (accountRegistration.blockHeight - cashaccount.blockModifier);

					// Try to find the transaction in the pending transaction list.
					let pendingTransactionIndex = window.app.wallet.history.pending.findIndex((obj => obj.transaction == window.app.wallet.registrationTxId));

					// Update the history entry.
					// TODO: Properly handle collisions.
					window.app.wallet.history.pending[pendingTransactionIndex].text = 'Registered ' + window.app.wallet.account.emoji + ' ' + window.app.wallet.account.name + '<small>#' + window.app.wallet.account.number + '</small>';
					window.app.wallet.history.pending[pendingTransactionIndex].translation = 'views.home.accountConfirmed';

					// Copy the transaction to confirmed history.
					window.app.wallet.history.entries.push(window.app.wallet.history.pending[pendingTransactionIndex]);

					// Remove it from the list of pending transactions.
					window.app.wallet.history.pending.splice(pendingTransactionIndex, 1);

					// Mark this transaction as processed so we do not change it in the future.
					window.app.wallet.history.transactions[window.app.wallet.registrationTxId] = true;

					// Update the cash account presentation on the receive view and in the drawer.
					await window.app.updateAccountPresentation();

					// If we're supposed to make a backup..
					if(window.app.config.backup.cloud)
					{
						// Make or update the backup now.
						await window.app.writeCloudBackup();
					}

					// Check that a transaction was confirmed.
					newTransactions = true;
					newValue = true;
				}
			}

			// Wait for any rate updates to settle before moving on.
			await window.app.updatingCurrencyRates;

			// get the rate for our fiat currency.
			let rate = window.app.currencyRates.filter(token => token.code === window.app.config.currency.current)[0].rate;

			// For each transaction in the history..
			for(let index in data)
			{
				// .. if we have not processed this transaction before..
				if(!window.app.wallet.history.transactions[data[index].txid])
				{
					// Try to find the transaction in the pending transaction list.
					let pendingTransactionIndex = window.app.wallet.history.pending.findIndex((obj => obj.transaction == data[index].txid));

					// If the transaction is not yet in the pending list...
					if(pendingTransactionIndex == -1)
					{
						// Check if this transaction is too old to be parsed..
						if(moment.unix(data[index].time).isAfter(moment(window.app.wallet.birth)))
						{
							// Note that there is new transactions available..
							newTransactions = true;

							// .. and it is an incoming transaction.
							if(data[index].action == 'received')
							{
								// Note that there is new value available..
								newValue = true;

								// If the receive amount exactly matches and expected amount..
								if(typeof window.app.receiveExpectationSatoshis != 'undefined' && (data[index].amount == window.app.receiveExpectationSatoshis))
								{
									//TODO: Translate this.
									// Inform the user that funds was received.
									window.plugins.toast.showShortCenter('Received');

									// TODO: Figure out a way (OP_RETURN? Reverse lookups?) to identify the sender.
									window.app.wallet.history.pending.push
									(
										{
											timestamp: moment.unix(data[index].time).valueOf(),
											transaction: data[index].txid,
											type: 'received',
											icon: '--received',
											text: 'Request paid',
											value: window.app.receiveExpectationAmount,
											currency: window.app.receiveExpectationCurrency,
											translation: 'views.home.receivedText'
										}
									);

									// Close the receive view, which also clears the expectation.
									await window.app.closeView();
								}
								else
								{
									// TODO: Figure out a way (OP_RETURN? Reverse lookups?) to identify the sender.
									window.app.wallet.history.pending.push
									(
										{
											timestamp: moment.unix(data[index].time).valueOf(),
											transaction: data[index].txid,
											type: 'received',
											icon: '--received',
											text: 'Received money',
											value: (data[index].amount / 100000000 * rate),
											currency: window.app.config.currency.current,
											translation: 'views.home.receivedText'
										}
									);
								}
							}
							
							// .. and it is an outgoing transaction.
							if(data[index].action == 'sent')
							{
								window.app.wallet.history.pending.push
								(
									{
										timestamp: moment.unix(data[index].time).valueOf(),
										transaction: data[index].txid,
										type: 'sent',
										icon: '--sent',
										text: 'Sent',
										value: (data[index].amount / 100000000 * rate),
										currency: window.app.config.currency.current,
										translation: 'views.home.sentText'
									}
								);
							}
						}
					}

					// If confirmed in a block, mark this transaction as complete and don't update it in the future.
					if(data[index].confirmations > 0)
					{
						// Update the pending transaction index in case the transaction was just added.
						pendingTransactionIndex = window.app.wallet.history.pending.findIndex((obj => obj.transaction == data[index].txid));

						// Check if this transaction exist in the list of pending transactions..
						if(window.app.wallet.history.pending[pendingTransactionIndex])
						{
							// Note that there is new transactions settled.
							newTransactions = true;
							newValue = true;

							// Copy the transaction to confirmed history.
							window.app.wallet.history.entries.push(window.app.wallet.history.pending[pendingTransactionIndex]);

							// Remove it from the list of pending transactions.
							window.app.wallet.history.pending.splice(pendingTransactionIndex, 1);

							// Mark this transaction as processed so we do not change it in the future.
							window.app.wallet.history.transactions[data[index].txid] = true;
						}
					}
				}

				// Try to find the transaction in the pending transaction list.
				let pendingTransactionIndex = window.app.wallet.history.pending.findIndex((obj => obj.transaction == data[index].txid));
				let historyTransactionIndex = window.app.wallet.history.entries.findIndex((obj => obj.transaction == data[index].txid));
				
				// If the transaction is pending...
				if((pendingTransactionIndex != -1) && (typeof window.app.wallet.history.pending[pendingTransactionIndex].data == 'undefined'))
				{
					// .. add the transaction details.
					window.app.wallet.history.pending[pendingTransactionIndex].data = data[index];
				}
				
				// If the transaction is confirmed...
				if((historyTransactionIndex != -1) && ((typeof window.app.wallet.history.entries[historyTransactionIndex].data == 'undefined') || (window.app.wallet.history.entries[historyTransactionIndex].data.confirmations < data[index].confirmations)))
				{
					// .. add the transaction details.
					window.app.wallet.history.entries[historyTransactionIndex].data = data[index];
				}
			}

			// Update wallet balance if new transactions were discovered.
			if(newTransactions)
			{
				// If the changes is incoming funds or a transaction settling on-chain..
				if(newValue)
				{
					// Vibrate.
					navigator.vibrate(VIBRATE['ATTENTION']);

					// Play the receive/settle sound.
					if(window.app.config.sounds)
					{
						window.app.sounds.receive.play();
					}
				}
				
				// Sort history.
				window.app.wallet.history.entries.sort(window.app.compareTransactionTimestamps);

				// Store changes to local storage.
				localStorage.wallet = JSON.stringify(window.app.wallet);

				// Update the wallet balance and display.
				await window.app.updateWalletBalance();
			}

			// Update the transaction history display.
			await window.app.showTransactionHistory();

			// TODO: Integrate this better.
			await window.app.updateHistoryContactList();

			// Set the loading indicator to complete state.
			document.getElementById('status_history').className = 'row'
			document.getElementById('status_history').childNodes[0].className = 'icon fas fa-check'
		}
		catch(error)
		{
			// TODO: Update error message.
			console.log(error);
			throwDebug('Failed to update transaction history', error);
		}
	}

	async showTransactionHistory()
	{
		try
		{
			// Assume we have no pending transctions.
			let pendingCount = 0
			let pendingList = "";

			// Assume we have no settled transactions.
			let transactionCount = 0;
			let transactionList = "";
			let transactionHeader = "";
			
			// Only update history if there is a wallet to update history for.
			if(typeof window.app.wallet !== 'undefined')
			{
				// For each pending transaction..
				for(let index in window.app.wallet.history.pending)
				{
					if(window.app.wallet.history.pending[index] !== null)
					{
						try
						{
							let name = window.app.wallet.history.pending[index].text;
							let value = window.app.wallet.history.pending[index].value
							
							// Check if the name is a valid cash account identifier.
							if(cashaccount.identifierRegexp.test(name))
							{
								// Match the name to a parser to get the account parts.
								let accountParts = name.match(cashaccount.identifierRegexp);
								
								//
								name = accountParts[1] + ' <small>#' + accountParts[2] + '</small>';
							}
							
							if(window.app.wallet.history.pending[index].value !== null)
							{
								value = Number(value).toLocaleString(Intl.DateTimeFormat().resolvedOptions().locale, { style: 'currency', currency: window.app.wallet.history.pending[index].currency });
							}
							
							// If this is the first pending transaction, add the pending header.
							if(pendingCount == 0)
							{
								pendingList = "<li class='system header pending'><span class='user' data-string='views.home.pendingTitle'>Pending</span></li>";
							}

							//  Increase the pending counter and add the pending entry.
							pendingCount += 1;
																																																	//TODO: Translate this.
							pendingList += "<li class='" + window.app.wallet.history.pending[index].type + "' onclick=\"app.showTransactionDetails(" + index + ", true);\"><span class='icon " + window.app.wallet.history.pending[index].icon + "'>" + (window.app.wallet.history.pending[index].iconText || '') + "</span><span class='user'>" + name + "</span><span class='value'>" + (value || "") + "</span></li>";
						}
						catch(error)
						{
							console.log(error);
							console.log(window.app.wallet.history.pending);
						}
					}
				}

				// Sort history.
				window.app.wallet.history.entries.sort(window.app.compareTransactionTimestamps);

				//
				for(let index in window.app.wallet.history.entries)
				{
					if(window.app.wallet.history.entries[index] !== null)
					{
						// Check if this transaction is too old to be shown..
						if(moment.unix(window.app.wallet.history.entries[index].timestamp).isAfter(moment(window.app.wallet.birth)))
						{
							try
							{
								let name = window.app.wallet.history.entries[index].text;
								let value = window.app.wallet.history.entries[index].value
								let newTransactionHeader;

								// Check if the name is a valid cash account identifier.
								if(cashaccount.identifierRegexp.test(name))
								{
									// Match the name to a parser to get the account parts.
									let accountParts = name.match(cashaccount.identifierRegexp);

									//
									name = accountParts[1] + ' <small>#' + accountParts[2] + '</small>';
								}

								if(window.app.wallet.history.entries[index].value !== null)
								{
									value =  Number(value).toLocaleString(Intl.DateTimeFormat().resolvedOptions().locale, { style: 'currency', currency: window.app.wallet.history.entries[index].currency });
								}

								// Calculate a transaction header based on time since the transaction.
								// If the transaction is the same day as today, mark the header green.
								if(moment().diff(moment(window.app.wallet.history.entries[index].timestamp), 'hours') < 1)
								{
									newTransactionHeader = "<li class='system header today'><span class='user'>Recently</span></li>";
								}
								else
								{
									newTransactionHeader = "<li class='system header'><span class='user'>" + moment(window.app.wallet.history.entries[index].timestamp).fromNow() + "</span></li>";
								}

								if(newTransactionHeader != transactionHeader)
								{
									transactionHeader = newTransactionHeader;
									transactionList += transactionHeader;
								}
								
								transactionCount += 1;
																																																	//TODO: Translate this.
								transactionList += "<li class='" + window.app.wallet.history.entries[index].type + "' onclick=\"app.showTransactionDetails(" + index + ");\"><span class='icon " + window.app.wallet.history.entries[index].icon + "'>" + (window.app.wallet.history.entries[index].iconText || '') + "</span><span class='user'>" + name + "</span><span class='value'>" + (value || "") + "</span></li>";
							}
							catch(error)
							{
								console.log(error);
								console.log(window.app.wallet.history.entries);
							}
						}
					}
				}
				
				document.getElementById('transaction_pending').innerHTML = pendingList;
				document.getElementById('transaction_history').innerHTML = transactionList;
			}
		}
		catch(error)
		{
			console.log(error);
			throwDebug('Failed to show transaction history', error);
		}
	}

	async showTransactionDetails(index, pending = false)
	{
		try
		{
			// Initialize a local variable to hold for the transction.
			let transaction;
			let intent;

			let expectation = 0;
			
			// Make a copy of the transaction that was requested.
			if(pending)
			{
				intent = window.app.wallet.history.pending[index];
			}
			else
			{
				intent = window.app.wallet.history.entries[index];
			}

			if(typeof intent.data == 'undefined')
			{
				// Inform the user that the transaction details are not available.
				window.plugins.toast.showShortCenter('Transaction details not available.');
				
				// Return false to indicate that no action was taken.
				return false;
			}
			else
			{
				transaction = intent.data;
			}

			/**
			 * 										{
			 *											timestamp: moment.unix(data[index].time).valueOf(),
			 *											transaction: data[index].txid,
			 *											type: 'received',
			 *											icon: '--received',
			 *											text: 'Received money',
			 *											value: (data[index].amount / 100000000 * rate),
			 *											currency: window.app.config.currency.current,
			 *											translation: 'views.home.receivedText'
			 **/

			// Hide the intent until we can be sure that we have the information necessary.
			document.querySelector('#transaction .intent').style.display = 'none';

			//
			document.querySelector('#transaction .intent .scope').innerHTML = 'local';

			if(intent.type == 'received')
			{
				document.querySelector('#transaction .intent .direction').innerHTML = 'From:';
			}
			else if(intent.type == 'sent')
			{
				document.querySelector('#transaction .intent .direction').innerHTML = 'To:';
			}
			else
			{
				document.querySelector('#transaction .intent .direction').innerHTML = 'local';
			}

			if(intent.type == 'sent')
			{
				// TODO: Check for an actual intent, instead of assuming we have data on outgoing only.
				// Show the intent now that we know that we have some data to show.
				document.querySelector('#transaction .intent').style.display = 'flex';
			}
			
			// Check if the name is a valid cash account identifier.
			if(cashaccount.identifierRegexp.test(intent.text))
			{
				// Match the name to a parser to get the account parts.
				let accountParts = intent.text.match(cashaccount.identifierRegexp);

				//
				document.querySelector('#transaction .intent .icon').className = 'icon emoji';
				document.querySelector('#transaction .intent .icon').innerHTML = intent.iconText;
				document.querySelector('#transaction .intent .identifier').innerHTML = '<span>' + accountParts[1] + '</span><small>#' + accountParts[2] + '</small>';
			}
			else
			{
				//
				document.querySelector('#transaction .intent .icon').className = 'icon fas fa-qrcode';
				document.querySelector('#transaction .intent .icon').innerHTML = '';

				if(transaction.addressTo)
				{
					// Recipient address.
					document.querySelector('#transaction .intent .identifier').innerHTML = '<span>Address</span><small>' + transaction.addressTo.substring(0, 21) + '..</small>';
				}
				else
				{
					if(typeof transaction.outputs == 'object' && transaction.outputs.length == 1)
					{
						// Recipient address.
						document.querySelector('#transaction .intent .identifier').innerHTML = '<span>Address</span><small>' + transaction.outputs[0].address.substring(0, 21) + '..</small>';
					}
					else
					{
						console.log(transaction);
					}
				}
			}

			// Update the fiat amount and exchange rate.
			// NOTE: This string contains non-breaking spaces. They look like normal spaces, so take care not to accidentally lose them.
			document.querySelector('#transaction .intent .amount_fiat').innerHTML = Number(intent.value).toLocaleString(Intl.DateTimeFormat().resolvedOptions().locale, { style: 'currency', currency: intent.currency });
			document.querySelector('#transaction .intent .amount_rate').innerHTML = (intent.value / (transaction.amount / 100000000)).toFixed(1) + '&#160;' + intent.currency + "/BCH";

			// Update the timestamp.
			// NOTE: This string contains non-breaking spaces. They look like normal spaces, so take care not to accidentally lose them.
			document.querySelector('#transaction .intent .date_day').innerHTML = moment(intent.timestamp).format('MMMM&#160;Do&#160;YYYY');
			document.querySelector('#transaction .intent .date_time').innerHTML = moment(intent.timestamp).format('hh:mm:ss');

			//
			let time_deviation_in_minutes = moment.unix(transaction.time).diff(moment(intent.timestamp), 'minutes');

			// Show verfied if within 45 minutes, warning if more than 45 minutes but within 3 hours, and an error otherwise.
			if(time_deviation_in_minutes < 45)
			{
				document.querySelector('#transaction .intent .date .icon').className = 'icon fas fa-clock verified';
			}
			else if(time_deviation_in_minutes >= 45 && time_deviation_in_minutes < 180)
			{
				expectation = Math.max(1, expectation);
				document.querySelector('#transaction .intent .date .icon').className = 'icon fas fa-exclamation-triangle warning';
			}
			else
			{
				expectation = Math.max(2, expectation);
				document.querySelector('#transaction .intent .date .icon').className = 'icon fas fa-exclamation-circle error';
			}

			// Store the current exchange rate to compare with for unconfirmed transactions.
			let expected_exchange_rate = { rate: window.app.currencyRates.filter(token => token.code === intent.currency)[0].rate };

			// If this transaction is confirmed..
			if(transaction.confirmations > 0)
			{
				// TODO: Copy the version we end up with from above..
				expected_exchange_rate = await window.app.walletManager.getFiatRateAsync({ code: intent.currency, ts: moment(intent.timestamp), provider: 'BitPayBCH' });

				// Store the difference in minutes between the transactions first-seen timestamp and the backends timestamp for when it fetched the data.
				let fiat_timestamp_diff_in_minutes = moment(intent.timestamp).diff(moment(expected_exchange_rate.fetchedOn), 'minutes');
				
				// If the timestamp differs by more than 10 minutes, but less than an hour..
				if(fiat_timestamp_diff_in_minutes > 10 && fiat_timestamp_diff_in_minutes < 60)
				{
					expectation = Math.max(1, expectation);
					document.querySelector('#transaction .intent .fiat .icon').className = 'icon fas fa-exclamation-triangle warning';
				}
				// If the timestamp differs by more than an hour..
				else if(fiat_timestamp_diff_in_minutes >= 60)
				{
					expectation = Math.max(2, expectation);
					document.querySelector('#transaction .intent .fiat .icon').className = 'icon fas fa-exclamation-circle error';
				}
			}

			// Calculate how many percent different the claimed exchange rate is compared with the expected.
			let fiat_rate_diff_in_percent =  100 * Math.abs((expected_exchange_rate.rate) / (intent.value / (transaction.amount / 100000000)) - 1);
			console.log('FRD:' + fiat_rate_diff_in_percent);

			// If the calculate difference in exchange rate is not a number (missing data)...
			if(isNaN(fiat_rate_diff_in_percent))
			{
				expectation = Math.max(1, expectation);
				document.querySelector('#transaction .intent .fiat .icon').className = 'icon fas fa-exclamation-triangle warning';
			}
			// If the exchange rate difference is less than 2 percent..
			else if(fiat_rate_diff_in_percent < 2)
			{
				document.querySelector('#transaction .intent .fiat .icon').className = 'icon fas fa-coins verified';
			}
			// If the fiat exchange rate differs between 2% and 5%..
			else if(fiat_rate_diff_in_percent >= 2 && fiat_rate_diff_in_percent <= 5)
			{
				expectation = Math.max(1, expectation);
				document.querySelector('#transaction .intent .fiat .icon').className = 'icon fas fa-exclamation-triangle warning';
			}
			// If the fiat exchange rate differs more than 5 percent...
			else if(fiat_rate_diff_in_percent > 5)
			{
				expectation = Math.max(2, expectation);
				document.querySelector('#transaction .intent .fiat .icon').className = 'icon fas fa-exclamation-circle error';
			}

			if(transaction.txid)
			{
				//
				document.querySelector('#transaction .specification .technical_transaction_hash').setAttribute('onclick', 'app.copyToClipboard("' + transaction.txid + '")');
				document.querySelector('#transaction .specification .technical_transaction_hash').innerHTML = transaction.txid.replace(/.{8}/g, "$&" + " ");
				//document.querySelector('#transaction .specification .technical_block_hash').innerHTML = transaction.blockHash.replace(/.{8}/g, "$&" + " ");
			}
			else
			{
				//
			}

			// Amount in BCH
			document.querySelector('#transaction .specification .technical_amount').innerHTML = (transaction.amount / 100000000) + "&nbsp;BCH";

			if(transaction.addressTo)
			{
				// Recipient address.
				document.querySelector('#transaction .specification .technical_address').innerHTML = transaction.addressTo.replace(new RegExp(".{" + Math.floor(transaction.addressTo.length / 2) + "}", "g"), "$&" + " ");
			}
			else
			{
				if(typeof transaction.outputs == 'object' && transaction.outputs.length == 1)
				{
					// Recipient address.
					document.querySelector('#transaction .specification .technical_address').innerHTML = transaction.outputs[0].address.replace(new RegExp(".{" + Math.floor(transaction.outputs[0].address.length / 2) + "}", "g"), "$&" + " ");
				}
				else
				{
					console.log(transaction);
				}
			}
		
			// Size, Fee?
			document.querySelector('#transaction .specification .technical_size').innerHTML = transaction.size + "&nbsp;bytes";
			document.querySelector('#transaction .specification .technical_fee').innerHTML = "<span>" + (transaction.fees / 100000000) + "&nbsp;BCH</span><small>" + (transaction.fees / transaction.size).toFixed(2) + "&nbsp;satoshi/byte</small>";

			// Blockheight, confirmations?
			if(transaction.confirmations > 0)
			{
				document.querySelector('#transaction .specification .technical_block_confirmations').innerHTML = "<span>" + transaction.blockheight + "</span><small>" + transaction.confirmations + "&nbsp;confirmations</small>";
			}
			else
			{
				document.querySelector('#transaction .specification .technical_block_confirmations').innerHTML = "<b>Unconfirmed</b>";
			}

			// Transction timestamp.
			document.querySelector('#transaction .specification .block_day').innerHTML = moment.unix(transaction.time).format('MMMM&#160;Do&#160;YYYY');
			document.querySelector('#transaction .specification .block_time').innerHTML = moment.unix(transaction.time).format('hh:mm:ss');

			// Update technical amount, technical recipient and block hash...
			//document.querySelector('#transaction .intent .scope').innerHTML = 'local';

			// TODO: Handle this better.
			document.querySelector('#transaction .expectation').style.display = 'inline-block';

			// If expectations for the time and value of the transactions are met...
			if(expectation == 0)
			{
				// TODO: Handle this better.
				document.querySelector('#transaction .expectation').style.display = 'none';

				document.querySelector('#transaction .expectation').className = 'expectation help';
				document.querySelector('#transaction .expectation').innerHTML = "Time and value <b>are close to</b> expectations.";
			}
			// If expectations for the time and value are somewhat off...
			else if(expectation == 1)
			{
				document.querySelector('#transaction .expectation').className = 'expectation warning';
				document.querySelector('#transaction .expectation').innerHTML = "Time or value <b>differs</b> from expectations.";
			}
			// If the expectations for the time and value are just plain wrong...
			else
			{
				document.querySelector('#transaction .expectation').className = 'expectation error';
				document.querySelector('#transaction .expectation').innerHTML = "Time or value <b>violates</b> expectation.";
			}

			// Show the transaction.
			window.app.showView('transaction');
		}
		catch(error)
		{
			throwDebug('Failed to show transaction details: ', error);
		}
	}
	
	async updateWalletBalance()
	{
		try
		{
			// If we have a wallet..
			if(typeof window.app.wallet != 'undefined')
			{
				// Set the loading indicator to working state.
				document.getElementById('status_balance').className = 'loading row'
				document.getElementById('status_balance').childNodes[0].className = 'icon fas fa-sync-alt'

				// Wait for any updates to the curreny rate.
				await window.app.updatingCurrencyRates;

				// Get the new balance.
				window.app.wallet.balance = await window.app.walletManager.getBalanceAsync({});

				// Fetch the available value and exchange rate..
				let asset = window.app.wallet.balance.availableAmount / 100000000;
				let rate = window.app.currencyRates.filter(token => token.code === window.app.config.currency.current)[0].rate;
				
				// Calculate the current balance in the selected currency.
				let balance = (asset * rate);

				if(window.app.config.currency.current !== 'BCH' && window.app.config.currency.current !== 'BTC')
				{
					// Display it formatted according to the users locale.
					document.getElementById('balance').innerHTML = balance.toLocaleString(Intl.DateTimeFormat().resolvedOptions().locale, { style: 'currency', currency: window.app.config.currency.current });
				}
				else
				{
					// TODO: consider the  "cash" uBCH unit.
					//document.getElementById('balance').innerHTML = (balance * 1000000).toFixed(2) + "&nbsp;ȼ";

					// Display it formatted according to the the cryptocurrency.
					document.getElementById('balance').innerHTML = balance.toFixed(8) + "&nbsp;" + window.app.config.currency.current;
				}
				
				// Set the loading indicator to complete state.
				document.getElementById('status_balance').className = 'row'
				document.getElementById('status_balance').childNodes[0].className = 'icon fas fa-check'
			}
		}
		catch(error)
		{
			throwDebug('Failed to update wallet balance', error);
		}
	}

	async setupConfig(event)
	{
		try
		{
			// Parse locale data from the device.
			let localeData = JSON.parse(event);

			// Initialize the config with a clean template.
			window.app.config = templates.config;

			// Store the regional language as the current language.
			window.app.config.language.current = localeData.languageCode;
			window.app.config.language.regional = localeData.languageCode;

			// Store the regional curreny as the current currency.
			window.app.config.currency.current = localeData.currencyCode;
			window.app.config.currency.regional = localeData.currencyCode;
			window.app.config.currency.selected = localeData.currencyCode;

			// Mark the regional currency as a relevant currency.
			window.app.config.currency.relevant[localeData.currencyCode] = true;

			// Save the config to storage.
			localStorage.config = JSON.stringify(window.app.config);

			// Wait out any current attempts to parse the old configuration.
			await window.app.parsingConfiguration;

			// Manually trigger the parsing of the new configuration.
			await window.app.parseConfig();
		}
		catch(error)
		{
			throwDebug('Failed to complete setup config', error);
		}
	}

	async loadLocaleNameData()
	{
		// Load the language name data.
		window.app.languageContext = require.context('cldr-localenames-modern/main/', true, /\/[a-z]{2}\/languages\.json$/);

		// Load the currency name data.
		window.app.currencyContext = require.context('cldr-numbers-modern/main/', true, /\/[a-z]{2}\/currencies\.json$/);
	}
	
	async loadLocaleNames()
	{
		try
		{
			// Initialize empty language and curreny name holders.
			window.app.languageNames = {};
			window.app.currencyNames = {};

			// Parse all language names.
			for(let currentLanguageCode in window.app.availableTranslations)
			{
				let languagePackage = './' + currentLanguageCode + '/languages.json';
				let languageNames = window.app.languageContext(languagePackage);
				
				//
				window.app.languageNames[currentLanguageCode] = languageNames.main[currentLanguageCode].localeDisplayNames.languages;
			}
			
			// Parse all currency names.
			for(let currentLanguageCode in window.app.availableTranslations)
			{
				let currencyPackage = './' + currentLanguageCode + '/currencies.json';
				let currencyNames = window.app.currencyContext(currencyPackage);
				
				// Initialize with cryptoname support.
				window.app.currencyNames[currentLanguageCode] = 
				{
					BTC: 'Bitcoin',
					BCH: 'Bitcoin Cash',
				};
				
				//
				for(let currencyCode in currencyNames.main[currentLanguageCode].numbers.currencies)
				{
					window.app.currencyNames[currentLanguageCode][currencyCode] = currencyNames.main[currentLanguageCode].numbers.currencies[currencyCode]['displayName-count-other'];
				}
			}
		}
		catch(error)
		{
			throwDebug('Failed to load locale names', error);
		}
	}
	
	async updateLanguageSelector()
	{
		try
		{
			//
			let template = document.querySelector('#translationTemplate');
			
			// Clear out all previous translation from the list.
			document.getElementById('translationList').innerHTML = "";
			
			// Initialize an empty set of nodes
			let translationNodes = [];
			
			// Create all translation entries.
			for(let currentTranslationCode in window.app.availableTranslations)
			{
				// Only add entries that aren't english.
				if(currentTranslationCode !== 'en')
				{
					//
					let currentNode = document.importNode(template.content, true);
					let currentName = window.app.languageNames[window.app.config.language.current][currentTranslationCode];
					let currentRatio = ((window.app.availableTranslations[currentTranslationCode] / window.app.availableTranslations['en']) * 100).toFixed(0) + "%";

					//
					currentNode.querySelector('.title').innerHTML = currentName;
					currentNode.querySelector('.icon').innerHTML = currentRatio;
					currentNode.querySelector('.row').setAttribute('onclick', "app.setLanguage('" + currentTranslationCode + "', '" + currentName + "'); app.closeView();");

					translationNodes.push({ name: currentName, node: currentNode });
				}
			}
			
			// Sort translations by name.
			translationNodes.sort(window.app.compareObjectNames);
			
			// For each translation...
			for(let index in translationNodes)
			{
				// Add it to the translation list.
				document.getElementById('translationList').appendChild(translationNodes[index].node);
			}
		}
		catch(error)
		{
			throwDebug('Failed to update language selector', error);
		}
	}

	async toggleTranslation()
	{
		try
		{
			// If the we are currently using a translation..
			if(window.app.config.language.current == window.app.config.language.selected)
			{
				// Revert to the original language.
				window.app.config.language.current = window.app.config.language.native;
			}
			else
			{
				// Return to the secondary translation.
				window.app.config.language.current = window.app.config.language.selected;
			}
			
			// Apply translation.
			await window.app.translateApplication(translations.strings);
		}
		catch(error)
		{
			throwDebug('Failed to toggle application translation', error);
		}
	}

	async setLanguage(code)
	{
		try
		{
			// Store the language as the selected and current langauge.
			window.app.config.language.current = code;
			window.app.config.language.selected = code;
			
			// Save the config to storage.
			localStorage.config = JSON.stringify(window.app.config);
			
			// Apply translation.
			await window.app.translateApplication(translations.strings);
		}
		catch(error)
		{
			throwDebug('Failed to set application language', error);
		}
	}

	async translateApplication()
	{
		try
		{
			// If the users language is not available, fall back to english.
			if(!window.app.availableTranslations[window.app.config.language.current])
			{
				window.app.config.language.current = 'en';
			}
			
			// Update the langauge and currency names.
			await window.app.updateLanguageNames(window.app.config.language.current);
			await window.app.updateCurrencyNames(window.app.config.language.current);
			
			// Update the language and currency selectors.
			await window.app.updateLanguageSelector()
			await window.app.updateCurrencySelector()
			
			// Apply translation.
			await window.app.translateStrings(translations.strings);
		}
		catch(error)
		{
			throwDebug('Failed to translate application interface', error);
		}
	}

	async updateLanguageNames()
	{
		try
		{
			// Get a list of nodes that print out the language name
			let nameNodes = document.getElementsByClassName('languageName');
			let codeNodes = document.getElementsByClassName('languageCode');
			
			// Update the display name.
			for(let index in nameNodes)
			{
				if(typeof nameNodes[index] == 'object')
				{
					nameNodes[index].innerHTML = window.app.languageNames[window.app.config.language.current][window.app.config.language.current];
				}
			}
			
			// Update the language code.
			for(let index in codeNodes)
			{
				if(typeof codeNodes[index] == 'object')
				{
					codeNodes[index].innerHTML = window.app.config.language.current;
				}
			}
			
			// Update the name of the default English based on the current language.
			document.getElementById('defaultLanguageName').innerHTML = window.app.languageNames[window.app.config.language.current]['en'];
		}
		catch(error)
		{
			throwDebug('Failed to update language names', error);
		}
	}

	async updateHomeTitle()
	{
		try
		{
			// Set the app version in the header.
			document.getElementById('app_title').innerHTML = "<small>Cashual Beta  <small>v" + AppVersion.version + "</small></small>";
		}
		catch(error)
		{
			throwDebug("Failed to update home title: " + JSON.stringify(error));
		}
	}

	async setCurrency(code)
	{
		try
		{
			let scope = await window.app.parentView();

			// If this was called from a global perspective (!send, !receive)..
			if(scope !== 'send' && scope !== 'receive')
			{
				//
				window.app.config.currency.current = code;

				// If this currency is not in the list of relevant currencies..
				if(!window.app.config.currency.relevant[code])
				{
					// Mark the currency as relevant.
					window.app.config.currency.relevant[code] = true;

					// Update the currency selector.
					window.app.updateCurrencySelector();
				}

				// Save the config to storage.
				localStorage.config = JSON.stringify(window.app.config);

				// Start the currency rate update in the background.
				window.app.updateCurrencyRates();

				// Update all references to the current currency by name.
				await window.app.updateCurrencyNames();
			}

			// If this was called from either global or the send screen (!receive)..
			if(scope !== 'receive')
			{
				// Update the send screen currency.
				document.getElementById('send_amount').setAttribute('data-currency', code);
				document.getElementById('send_currency').innerHTML = code;

				// Clear the send value.
				document.getElementById('send_amount').setAttribute('data-amount', 0);
				document.getElementById('send_amount').value = '';

				// Update the max send value.
				window.app.showMaxValue();
			}

			// If this was called from either global or the receive screen (!send)..
			if(scope !== 'send')
			{
				// Update the receive screen currency.
				document.getElementById('receive_amount').setAttribute('data-currency', code);
				document.getElementById('receive_currency').innerHTML = code;

				// Clear the receive value.
				document.getElementById('receive_amount').setAttribute('data-amount', 0);
				document.getElementById('receive_amount').value = '';

				// Reset the QR code the plain address.
				window.app.updateAddress();
			}
			
			// TODO: Update wallet balance.
			// TODO: Update wallet history? (or don't, if history should retain the currency information.)
		}
		catch(error)
		{
			throwDebug('Failed to set application currency', error);
		}
	}

	//
	async parseConfig()
	{
		try
		{
			// Only parse the configuration if we have managed to initialize it first.
			if(typeof window.app.config !== 'undefined')
			{
				// Check if we are using the non-default night theme..
				if(window.app.config.theme == 'night')
				{
					// .. and apply theme if so.
					document.body.setAttribute('nightmode', true);
				}

				// Update theme and sound descriptions
				await window.app.updateThemeDescriptions();
				await window.app.updateSoundDescriptions();

				// Update regional currency presentation.
				document.getElementById('currencyRegionalCode').innerHTML = window.app.config.currency.regional;

				// Update the send and receive screen currencies.
				document.getElementById('send_amount').setAttribute('data-currency', window.app.config.currency.current);
				document.getElementById('send_currency').innerHTML = window.app.config.currency.current;
				document.getElementById('receive_amount').setAttribute('data-currency', window.app.config.currency.current);
				document.getElementById('receive_currency').innerHTML = window.app.config.currency.current;

				// Update currency rate display.
				await window.app.updateHomeTitle()

				// Load translations first, as it will be used to choose which languages to support.
				await window.app.loadingTranslations;

				// Wait for name data, then load names for langauge and currencies.
				await window.app.loadingLocaleNameData;
				await window.app.loadLocaleNames();
				
				// Apply translation.
				await window.app.translateApplication();

				// TODO: it might be better to do this later.
				// Update the transaction history display.
				await window.app.showTransactionHistory()
				
				// Show the user interface.
				await window.app.display();

				// TODO: Find a way to play the real video animation instead.
				// Play the startup sound.
				if(window.app.config.sounds)
				{
					//window.app.sounds.startup.play();
				}

				// TODO: Remove me later.
				if(typeof window.app.config.backup == 'undefined')
				{
					window.app.config.backup = 
					{
						overwrite: false,
						cloud: false,
						encryption: false
					}
				}

				// Update the backup display settings.
				await window.app.parseBackupOverwrite();
				await window.app.parseBackupCloud();
				//await window.app.parseBackupEncryption();

				/*
				// TODO: Use actual settings.
				if(window.app.config.backup.encryption)
				{
					document.body.querySelector('#config_encrypted_backups > .icon').style.color = 'green';
					document.body.querySelector('#config_encrypted_backups > .icon').className = 'icon fas fa-check';
				}
				else
				{
					document.body.querySelector('#config_encrypted_backups > .icon').style.color = 'red';
					document.body.querySelector('#config_encrypted_backups > .icon').className = 'icon fas fa-times';
				}
				*/

				// If we haven't created a wallet yet..
				if(typeof localStorage.wallet === 'undefined')
				{
					// TODO: handle this better.
					window.app.updateHistoryContactList();

					// Update the transaction history.
					window.app.updateTransactionHistory()
				}
			}
		}
		catch(error)
		{
			throwDebug('Failed to parse application configuration', error);
		}
	}

	async toggleBackupOverwrite()
	{
		try
		{
			// Toggle the setting.
			window.app.config.backup.overwrite = !window.app.config.backup.overwrite;

			// Save the config to storage.
			localStorage.config = JSON.stringify(window.app.config);

			// Update the configuration display.
			await window.app.parseBackupOverwrite();
		}
		catch (error)
		{
			throwDebug('Failed to toggle backup overwrite setting:', error);
		}
	}
	
	async toggleBackupCloud()
	{
		try
		{
			// Toggle the setting.
			window.app.config.backup.cloud = !window.app.config.backup.cloud;

			// Save the config to storage.
			localStorage.config = JSON.stringify(window.app.config);

			// If we're supposed to make a backup..
			if(window.app.config.backup.cloud)
			{
				// Make or update the backup now.
				await window.app.writeCloudBackup();
			}

			// Update the configuration display.
			await window.app.parseBackupCloud();
		}
		catch (error)
		{
			throwDebug('Failed to toggle backup cloud setting:', error);
		}
	}
	
	async parseBackupOverwrite()
	{
		try
		{
			if(window.app.config.backup.overwrite)
			{
				document.body.querySelector('#config_touch_overwrite > .icon').style.color = 'green';
				document.body.querySelector('#config_touch_overwrite > .icon').className = 'icon fas fa-check';
				
				// TODO: Add translation.
				document.body.querySelector('#backupTouchTargets').innerHTML = 'any tag';
			}
			else
			{
				document.body.querySelector('#config_touch_overwrite > .icon').style.color = 'red';
				document.body.querySelector('#config_touch_overwrite > .icon').className = 'icon fas fa-times';
				
				// TODO: Add translation.
				document.body.querySelector('#backupTouchTargets').innerHTML = 'an empty tag';
			}
		}
		catch (error)
		{
			throwDebug('Failed to update display for backup overwrite setting.');
		}
	}

	async parseBackupCloud()
	{
		try
		{
			if(window.app.config.backup.cloud)
			{
				document.body.querySelector('#config_cloud_backups > .icon').style.color = 'green';
				document.body.querySelector('#config_cloud_backups > .icon').className = 'icon fas fa-check';
			}
			else
			{
				document.body.querySelector('#config_cloud_backups > .icon').style.color = 'red';
				document.body.querySelector('#config_cloud_backups > .icon').className = 'icon fas fa-times';
			}
		}
		catch (error)
		{
			throwDebug('Failed to update display for backup overwrite setting.');
		}
	}
	
	async copyToClipboard(text)
	{
		try
		{
			// Copy the text to the clipboard.
			cordova.plugins.cashual_wallet.copyToClipboard(text, passthrough, alert);

			//TODO: Translate this.
			window.plugins.toast.showShortBottom('Copied to clipboard.')
		}
		catch(error)
		{
			throwDebug('Failed to copy to clipboard', error);
		}
	}

	// Recursively calculate translation completeness.
	async loadTranslations(object = null, path = '')
	{
		try
		{
			// If we have not yet created a list of available translations..
			if(typeof window.app.availableTranslations == 'undefined')
			{
				// .. create an empty list of translations.
				window.app.availableTranslations = {};
			}
			
			// Iterate over each property at the current depth..
			for(let index in object)
			{
				// If the property is an object that does not hold translation string...
				if(typeof object[index] == 'object' && typeof object[index].en == 'undefined')
				{
					// Recursively parse this object.
					// NOTE: we await this here to prevent a race condition with the counters below.
					await window.app.loadTranslations(object[index], (path + "." + index));
				}
				// If the property does hold translation strings...
				else
				{
					// For each translation language..
					for(let currentLanguageCode in object[index])
					{
						// .. check if the string is translated..
						if(object[index][currentLanguageCode])
						{
							// .. and then either add the language as an available translation..
							if(typeof window.app.availableTranslations[currentLanguageCode] == 'undefined')
							{
								window.app.availableTranslations[currentLanguageCode] = 1;
							}
							// .. or increase the competeness rating of the translation.
							else
							{
								window.app.availableTranslations[currentLanguageCode] += 1;
							}
						}
					}
				}
			}
		}
		catch(error)
		{
			throwDebug('Failed to load translations', error);
		}
	}

	// Recursively translate all available strings.
	async translateStrings(object = null, path = '')
	{
		try
		{
			// Iterate over each property at the current depth..
			for(let index in object)
			{
				// If the property is an object that does not hold translation string...
				if(typeof object[index] == 'object' && typeof object[index].en == 'undefined')
				{
					// TODO: consider awaiting this to prevent browser issues..
					// Recursively parse this object.
					window.app.translateStrings(object[index], (path + "." + index));
				}
				// If the property does hold translation strings...
				else
				{
					// Store a local copy of the native and translated strings.
					let nativeString = object[index][window.app.config.language.native];
					let translatedString = object[index][window.app.config.language.current];
					
					// Choose which string to use based on availability.
					let currentString = (translatedString ? translatedString : nativeString);
					
					// Define the strings identity.
					let targetString = (path + "." + index).slice(1);
					
					// Find all nodes in the document that holds that identity..
					let targetNodes = document.querySelectorAll("*[data-string='" + targetString + "']");
					
					// For each node..
					for(let nodeIndex in targetNodes)
					{
						// .. check if this node has a string ..
						if(typeof targetNodes[nodeIndex] == 'object')
						{
							// .. then check what node type it is ...
							if(targetNodes[nodeIndex].tagName == 'INPUT')
							{
								// For inputs, apply the translation to the placeholder.
								targetNodes[nodeIndex].placeholder = currentString;
							}
							else
							{
								// For every other type, apply translation to their content.
								targetNodes[nodeIndex].innerHTML = currentString;
							}
						}
					}
				}
			}
		}
		catch(error)
		{
			throwDebug('Failed to translate strings', error);
		}
	}

	async updateThemeDescriptions()
	{
		try
		{
			let nodes = document.getElementsByClassName('themeName');
			
			for(let index in nodes)
			{
				if(typeof nodes[index] == 'object')
				{
					if(window.app.config.theme == 'day')
					{
						// Update theme descriptions.
						nodes[index].setAttribute('data-string', 'names.theme.day');
						nodes[index].innerHTML = translations.strings.names.theme.day[window.app.config.language.current];
					}
					else
					{
						// Update theme descriptions.
						nodes[index].setAttribute('data-string', 'names.theme.night');
						nodes[index].innerHTML = translations.strings.names.theme.night[window.app.config.language.current];
					}
				}
			}
		}
		catch(error)
		{
			throwDebug('Failed to apply application theme', error);
		}
	}

	async toggleTheme()
	{
		try
		{
			// Toggle the DOM attribute.
			document.body.toggleAttribute('nightmode');
			
			// Check which theme to use...
			if(document.body.getAttribute('nightmode') === null)
			{
				// Store config locally.
				window.app.config.theme = 'day';
			}
			else
			{
				// Store config locally.
				window.app.config.theme = 'night';
			}
			
			// Save the config to storage.
			localStorage.config = JSON.stringify(window.app.config);
			
			// Update the theme and sound names and descriptions.
			await window.app.updateThemeDescriptions();
			await window.app.updateSoundDescriptions();
		}
		catch(error)
		{
			throwDebug('Failed to toggle application theme', error);
		}
	}

	async updateSoundDescriptions()
	{
		try
		{
			let nodes = document.getElementsByClassName('soundName');
			
			for(let index in nodes)
			{
				if(typeof nodes[index] == 'object')
				{
					if(typeof window.app.config.sounds == 'undefined' || window.app.config.sounds !== true)
					{
						// Update sound descriptions.
						nodes[index].setAttribute('data-string', 'names.sounds.disabled');
						nodes[index].innerHTML = translations.strings.names.sounds.disabled[window.app.config.language.current];
					}
					else
					{
						// Update sound descriptions.
						nodes[index].setAttribute('data-string', 'names.sounds.enabled');
						nodes[index].innerHTML = translations.strings.names.sounds.enabled[window.app.config.language.current];
					}
				}
			}
		}
		catch(error)
		{
			throwDebug('Failed to apply application sound settings', error);
		}
	}

	async toggleSounds()
	{
		try
		{
			// Check which theme to use...
			if(typeof window.app.config.sounds == 'undefined' || window.app.config.sounds !== true)
			{
				window.app.config.sounds = true;
			}
			else
			{
				// Store config locally.
				window.app.config.sounds = false;
			}

			// Save the config to storage.
			localStorage.config = JSON.stringify(window.app.config);

			// Update the theme names and descriptions.
			await window.app.updateSoundDescriptions();
		}
		catch(error)
		{
			throwDebug('Failed to toggle application sounds', error);
		}
	}

	//
	async scanResult(error, text)
	{
		//console.log('result: ' + JSON.stringify(error) + " || " + JSON.stringify(text));
		try
		{
			// Hide and destroy the scanning preview/process.
			window.QRScanner.cancelScan();

			if(!error)
			{
				// If the scanned code is a bitcoin cash payment request..
				if(typeof text == 'string' && text.startsWith('bitcoincash:'))
				{
					let params = {};
					
					// remove the prefix.
					text = text.substring(12);

					// Check if there are optional parameters.
					if(text.indexOf('?') != -1)
					{
						let regexp = /[?&]([^=#]+)=([^&#]*)/g;
						let query = text.substring(text.indexOf('?'));
						let matches;

						// While there are params that can be matched with this regexp..
						while(matches = regexp.exec(query))
						{
							// Add them to the params object.
							params[matches[1]] = matches[2];
						}

						text = text.substring(0, text.indexOf('?'));

						/*
						// If there is a BIP70 payment request in the url..
						if(params.r)
						{
							console.log('Trying to fetch payment request: ' + params.r);
							let request = await window.app.walletManager.fetchPayProAsync({ payProUrl: params.r })
							console.log(request);

							// Returns { paypro.amount paypro.toAddress paypro.memo}
						}
						*/

						if(params.amount)
						{
							// Calculate the fiat equivalent of the max amount of satoshis.
							let rate = window.app.currencyRates.filter(token => token.code === window.app.config.currency.current)[0].rate;
							let fiat = (params.amount * rate).toFixed(2);
							
							// Set send amount
							document.getElementById('send_amount').setAttribute('data-amount', params.amount * 100000000);
							document.getElementById('send_amount').value = fiat;

							// TODO: Translate this.
							window.plugins.toast.showShortBottom('Read payment request');
						}
					}
				}

				/*
				// TEMP: copy signed content to clipboard.
				// I use this to vote in bitcoin unlimited.
				{
					console.log(window.app.walletManager.credentials);
					console.log(window.app.walletManager.credentials.getBaseAddressDerivationPath());

					// Fetch the main key. ("m/44'/0'/0'" + "/0/0")
					let derivedKey = window.app.walletManager.credentials.getDerivedXPrivKey().deriveChild(0, false).deriveChild(0, false);
					let address = derivedKey.publicKey.toAddress().toString();
					let cashaddress = bitcoreWallet.BitcoreCash.Address(derivedKey.publicKey.toAddress()).toCashAddress();
					let signature = bitcoreWallet.Bitcore.Message(text).sign(derivedKey.privateKey);

					console.log(derivedKey);
					console.log(address);
					console.log(cashaddress);

					let verification = "bitcoin-cli verifymessage '1NWAbNx9sLceMRVR9E2SzMSVrjWajni21D' '" + signature + "' '" + text + "'";

					window.app.copyToClipboard(signature);
				}
				*/

				// Vibrate.
				navigator.vibrate(VIBRATE['SUCCESS']);

				// Set the send screens recipient to the address.
				document.getElementById('recipientInput').value = text;
				
				// Trigger a changed recipient parsing of the sendTo value.
				await window.app.changeRecipient();

				// Close the interact view.
				await window.app.closeView();

				// Open the send view if not already opened.
				if(window.app.currentView != 'send')
				{
					// Open the send view.
					await window.app.showView('send');
				}
			}
		}
		catch(error)
		{
			throwDebug('Failed to parse scanner results', error);
		}
	}

	async onTouch(event)
	{
		try
		{
			// Notify that we detected a NFC tag..
			window.plugins.toast.showShortCenter('Detected tag.')

			// Vibrate.
			navigator.vibrate(VIBRATE['NOTIFY']);

			let empty = true;
			let cashual = false;
			let available = event.tag.maxSize;

			//alert(JSON.stringify(event));
			for(let index in event.tag.ndefMessage)
			{
				let payload = null;
				let tagtype = null;
				
				try
				{
					tagtype = nfc.bytesToString(event.tag.ndefMessage[index].type);
					payload = nfc.bytesToString(event.tag.ndefMessage[index].payload);

					if(tagtype == 'android.com:pkg' && payload == 'org.monsterbitar.wallet')
					{
						cashual = true;
					}
				}
				catch(error)
				{
					// Couldn't parse tagtype or payload, moving on..
				}

				// If this tag has content..
				if(event.tag.ndefMessage[index].payload.length > 0)
				{
					// Mark it as not empty.
					empty = false;
				}

				// Reduce available space with this message length.
				available -= event.tag.ndefMessage[index].payload.length;
			}

			console.log('Available space: ' + available);

			// If this tag is not empty..
			if(!empty)
			{
				// .. but we're configured to overwrite tags..
				if(window.app.currentView == 'backup_create')
				{
					if(window.app.config.backup.overwrite)
					{
						// Write the backup even though it has foreign content on it.
						window.app.writeTagBackup(event);
					}
					else
					{
						// Simply notify that we detected a NFC tag..
						window.plugins.toast.showLongCenter('Will not overwrite existing tag content.')
					}
				}
				// If it's not empty and it has our application record on it..
				else if(cashual)
				{
					// Parse the tag as a backup.
					window.app.readTagBackup(event);
				}
				// If it's not empty and is not made by us..
				else
				{
					// Simply notify that we detected a NFC tag..
					window.plugins.toast.showLongCenter('Cannot understand tag content.')
				}
			}
			// If this tag is empty..
			else
			{
				// Check if we are not on the create backup page..
				if(window.app.currentView != 'backup_create')
				{
					// Check if we have a working wallet to backup..
					if(window.app.walletManager.isComplete())
					{
						window.app.showView('backup_create');
					}
				}
				else
				{
					window.app.writeTagBackup(event);
				}
			}
		}
		catch(error)
		{
			throwDebug('Failed to parse touch results', error);
		}
	}

	async scanStart()
	{
		try
		{
			window.QRScanner.getStatus
			(
				function(status)
				{
					if(status.authorized)
					{
						// Remove marker as the user has now given us access.
						window.app.cameraDenied = false;

						// Start the scan
						window.QRScanner.scan(window.app.scanResult);
						
						// Make the webview background transparent;
						window.QRScanner.show();
					}
					else
					{
						if(window.app.cameraDenied)
						{
							// Remove marker to allow user to try again later.
							window.app.cameraDenied = false;

							// Close the view to indicate to user that we cannot scan without the permission.
							window.app.closeView();
						}
						else
						{
							// Not authorized, ask for permission and try again.
							window.QRScanner.prepare(window.app.scanStart);

							// Set a marker to prevent a recursive loop when user denies access.
							window.app.cameraDenied = true;
						}
					}
				}
			);
		}
		catch(error)
		{
			throwDebug('Failed to start scanner', error);
		}
	}

	async scanStop()
	{
		try
		{
			window.QRScanner.getStatus
			(
				function(status)
				{
					if(status.scanning)
					{
						// Hide the scanning preview/process.
						window.QRScanner.cancelScan();

						// TODO: This frees resources but makes showing the camera later much slower.
						// Destroy the scanning preview/process.
						//window.QRScanner.destroy();
						
						// Make the webview background opaque again;
						window.QRScanner.hide();
					}
					else
					{
						// Not started, nothing to do.
					}
				}
			);
		}
		catch(error)
		{
			throwDebug('Failed to start scanner', error);
		}
	}

	async createBackupData(encryptionPassword = false)
	{
		try
		{
			// Decode the master extended private key.
			let xPrivKey = bitcoreWallet.BitcoreCash.encoding.Base58Check.decode(JSON.parse(window.app.wallet.secret).xPrivKey);
			
			// Define the master extended private key parts.
			let version = Buffer.from('0488ADE4', 'hex');
			let depth = Buffer.from('00', 'hex');
			let fingerprint = Buffer.from('00000000', 'hex');
			let child = Buffer.from('00000000', 'hex');
			
			// Sum up the static metadata.
			let metadata = Buffer.concat([version, depth, fingerprint, child]);
			
			// Extract the entropy / secreys.
			let chain_code = xPrivKey.slice(13, 45);
			let key_data = xPrivKey.slice(45, 79);
			
			// Define the cash account as the registration transactions ID.
			let account = Buffer.from(window.app.wallet.registrationTxId, 'hex');

			// Merge the cash account reference, chain code and key data.
			let backupData = Buffer.concat([account, chain_code, key_data]);

			//
			if(encryptionPassword)
			{
				// TODO: Encrypt the backup data.
			}

			// 
			return backupData;
		}
		catch(error)
		{
			throwDebug('Failed to create backup data', error);
		}
	}
	
	async parseBackupData(backupData, encryptionPassword = false)
	{
		try
		{
			if(encryptionPassword)
			{
				// TODO: Attempt to decrypt the backupData.
			}

			// Define the master extended private key parts.
			let version = Buffer.from('0488ADE4', 'hex');
			let depth = Buffer.from('00', 'hex');
			let fingerprint = Buffer.from('00000000', 'hex');
			let child = Buffer.from('00000000', 'hex');
			
			// Sum up the static metadata.
			let metadata = Buffer.concat([version, depth, fingerprint, child]);
			
			// Extract the account and entropy / secrets.
			let account = backupData.slice(0, 32);
			let chain_code = backupData.slice(32, 64);
			let key_data = backupData.slice(64, 97);
			
			// Decode the master extended private key.
			let private_key = bitcoreWallet.BitcoreCash.encoding.Base58Check.encode(Buffer.concat([metadata, chain_code, key_data]));

			// Return the parsed backup object.
			return { account: account, xPrivKey: private_key };
		}
		catch(error)
		{
			throwDebug('Failed to parse backup data', error);
		}
	}
	
	async writeCloudBackup()
	{
		try
		{
			// Create the backup data.
			let backupBuffer = await window.app.createBackupData();

			// Store the backup as a key-value pair.
			let backupData = 
			{
				backup: backupBuffer.toString('hex'),
				config: window.app.config
				
			};

			// Store the backup in the cloud.
			cordova.plugin.cloudsettings.save(backupData, window.app.wroteCloudBackup, alert, true);
		}
		catch (error)
		{
			throwDebug('Failed to write cloud backup: ', error);
		}
	}

	async wroteCloudBackup()
	{
		// Notify that backup is completed.
		window.plugins.toast.showShortCenter('Scheduled cloud backup.')
	}

	async loadCloudBackup()
	{
		cordova.plugin.cloudsettings.load(window.app.restoreCloudBackup, alert);
	}
	
	async restoreCloudBackup(payload)
	{
		try
		{
			if(typeof payload !== 'undefined')
			{
				// Convert the backup from hex to a buffer.
				let backupBuffer = Buffer.from(payload.backup, 'hex');

				// Parse the backup payload.
				let backup = await window.app.parseBackupData(backupBuffer);

				// Replace current settings with backuped settings.
				if(payload.config)
				{
					window.app.config = payload.config;
				}

				// If the user is on the welcome screen..
				if(window.app.currentView == 'welcome')
				{
					// Change to the home screen.
					await window.app.showView('home');
					await window.app.initializeView('home');
				}

				//TODO: Translate this.
				window.plugins.toast.showLongCenter('Loading cloud backup.')

				// Load the backup.
				await window.app.loadBackup(backup.account, backup.xPrivKey);

				// Change to the newly restored backup.
				await window.app.createWallet(window.app.backup.account.identifier, window.app.backup.secret.key, window.app.backup.account.identifier, false);

				// Discard backup information.
				delete window.app.backup;
				delete window.app.backupAction;
				delete window.app.backupManager;

				// Update transaction balance and history.
				await window.app.updateTransactionHistory(true, true);
			}
		}
		catch (error)
		{
			throwDebug('Failed to restore cloud backup: ', error);
		}
	}

	async readTagBackup(event)
	{
		console.log('TAG', event);
		try
		{
			// Notify that we are parsing a backup..
			window.plugins.toast.showShortCenter('Parsing backup tag.')

			if((nfc.bytesToString(event.tag.ndefMessage[0].payload) == 'org.monsterbitar.wallet') && (event.tag.ndefMessage[1].tnf == ndef.TNF_UNKNOWN))
			{
				// Parse the backup payload.
				let backup = await window.app.parseBackupData(Buffer.from(event.tag.ndefMessage[1].payload));

				// If the user is on the scan screen..
				if(window.app.currentView == 'interact')
				{
					// Close the scan screen.
					await window.app.closeView();
				}

				// If this is the same key as we are currently using..
				if(typeof window.app.wallet !== 'undefined' && backup.xPrivKey == JSON.parse(window.app.wallet.secret).xPrivKey)
				{
					// Show the backup restore screen.
					await window.app.showView('backup_verify');

					// Load the backup.
					await window.app.loadBackup(backup.account, backup.xPrivKey);
				}
				else
				{
					// Show the backup restore screen.
					await window.app.showView('backup_restore');

					// Load the backup.
					await window.app.loadBackup(backup.account, backup.xPrivKey);
				}
			}
		}
		catch(error)
		{
			throwDebug('Failed to read tag backup', error);
		}
	}

	async writeTagBackup(event)
	{
		console.log('Writing backup to tag...');

		// Create the backup data.
		let backupData = await window.app.createBackupData();
		
		// Create a backup and application record.
		let backupRecord = ndef.record(ndef.TNF_UNKNOWN, null, null, [...backupData]);
		let applicationRecord = ndef.androidApplicationRecord('org.monsterbitar.wallet');

		// NOTE: Some phones are unable to write messages larger than a given size.
		//       The size appears to by around 256 bytes, but I've been unable to
		//       accurately measure the size since the nfc library adds metadata.

		// Write the application and backup record to the tag.
		nfc.write([applicationRecord, backupRecord], passthrough, alert);

		// Update last verified timestamp.
		window.app.wallet.lastVerified = moment.valueOf();

		// Notify that backup is completed.
		window.plugins.toast.showShortCenter('Wrote backup to tag.')
	}

	async verifyBackup()
	{
		try
		{
			// Create a history entry.
			window.app.wallet.history.entries.push({ timestamp: moment().valueOf(), transaction: 1, type: 'system', icon: '--verified', text: 'Verified backup', value: null, currency: null, translation: 'views.home.backupVerified' });
			
			// Update last verified timestamp.
			window.app.wallet.lastVerified = moment.valueOf();
			
			// Store changes to local storage.
			localStorage.wallet = JSON.stringify(window.app.wallet);

			// Close the verify backup dialog.
			window.app.closeView();

			// Update visual display.
			window.app.updateTransactionHistory();
			
			// Notify that backup is completed.
			window.plugins.toast.showShortCenter('Verified backup.')
		}
		catch(error)
		{
			throwDebug('Failed to verify backup: ' + JSON.stringify(error));
		}
	}

	async sweepWallet(fromWallet, toAddress)
	{
		try
		{
			// Check if the backup is functional..
			if(fromWallet.isComplete())
			{
				// Initialize parameters needed to ask for max sendable amount.
				let sendMaxOptions = 
				{
					feePerKb: window.app.config.feeRatePerKB,
					excludeUnconfirmedUtxos: false,
					returnInputs: false
				};
				
				// Ask the backend for a transaction structure for emptying the backup.
				let maxTransaction = await fromWallet.getSendMaxInfoAsync(sendMaxOptions);
				
				// Configure transaction proposal options.
				let sweepBackupOptions = 
				{
					feePerKb: window.app.config.feeRatePerKB,
					outputs:
					[
						{
							toAddress: toAddress,
							amount: maxTransaction.amount
						}
					]
				}
				
				if(maxTransaction.amount <= 0)
				{
					// Return no to indicate that there was no money to sweep.
					return false;
				}
				else
				{
					// Create a proposal structure.
					let proposal = await fromWallet.createTxProposalAsync(sweepBackupOptions);
					
					// Publish the proposal with the backend (part of the multisig workflow).
					let published = await fromWallet.publishTxProposalAsync({ txp: proposal });
					
					// Sign the published proposal with our keys, which should complete the 1-of-1 process.
					let signed = await fromWallet.signTxProposalAsync(published);
					
					// Broadcast the signed transaction by sending the signed proposal to the backend.
					let broadcast = await fromWallet.broadcastTxProposalAsync(signed);

					// Return the amount swept and transaction id to indicate we successfully swept the wallet.
					return { txid: broadcast.txid, amount: maxTransaction.amount };
				}
			}
			else
			{
				console.log('Not sweeping old account: signing keys.');
			}
		}
		catch(error)
		{
			throwDebug("Failed to sweep old account: " + JSON.stringify(error));
		}
	}

	async sweepFromBackup()
	{
		try
		{
			let sweepStatus = await window.app.sweepWallet(window.app.backupManager, window.app.wallet.address);
			
			if(sweepStatus === false)
			{
				// Vibrate.
				navigator.vibrate(VIBRATE['ERROR']);
				
				//TODO: Translate this.
				window.plugins.toast.showLongCenter('There was no funds to sweep.')
			}
			else
			{
				// Vibrate.
				navigator.vibrate(VIBRATE['SUCCESS']);
				
				//TODO: Translate this.
				window.plugins.toast.showShortCenter('Swept backup.')
				
				// Play the sending sound.
				if(window.app.config.sounds)
				{
					window.app.sounds.send.play();
				}
				
				// Calculate the fiat equivalent of the max amount of satoshis.
				let rate = window.app.currencyRates.filter(token => token.code === window.app.config.currency.current)[0].rate;
				let fiat = (sweepStatus.amount / 100000000 * rate);
				
				// Store an outgoing entry in the wallet history.
				window.app.wallet.history.pending.unshift
				(
					{
						timestamp: moment().valueOf(),
						transaction: sweepStatus.txid,
						type: 'received',
						icon: '--backup',
						iconText: '',
						text: 'Swept backup',
						value: fiat,
						currency: window.app.config.currency.current
					}
				);
			}
		}
		catch(error)
		{
			throwDebug("Failed to sweep backup: " + JSON.stringify(error));
		}
	}

	async sweepToBackup(sweepStatus)
	{
		try
		{
			if(sweepStatus === false)
			{
				// Vibrate.
				navigator.vibrate(VIBRATE['ERROR']);
				
				//TODO: Translate this.
				window.plugins.toast.showLongCenter('There was no funds to sweep.')
			}
			else
			{
				// Vibrate.
				navigator.vibrate(VIBRATE['SUCCESS']);
				
				//TODO: Translate this.
				window.plugins.toast.showShortCenter('Swept backup.')
				
				// Play the sending sound.
				if(window.app.config.sounds)
				{
					window.app.sounds.send.play();
				}

				// Calculate the fiat equivalent of the max amount of satoshis.
				let rate = window.app.currencyRates.filter(token => token.code === window.app.config.currency.current)[0].rate;
				let fiat = (sweepStatus.amount / 100000000 * rate);

				// Store an outgoing entry in the wallet history.
				window.app.wallet.history.pending.unshift
				(
					{
						timestamp: moment().valueOf(),
						transaction: sweepStatus.txid,
						type: 'received',
						icon: '--backup',
						iconText: '',
						text: 'Swept wallet',
						value: fiat,
						currency: window.app.config.currency.current
					}
				);
			}
		}
		catch(error)
		{
			throwDebug("Failed to sweep backup: " + JSON.stringify(error));
		}
	}

	async loadBackup(account, privateKey)
	{
		try
		{
			let backup = 
			{
				account: false,
				address: false,
				balance: false,
				secret:
				{
					key: privateKey
				}
			};

			if(window.app.currentView != 'home')
			{
				let view = document.querySelector('#' + window.app.currentView);

				view.querySelector('.backup_type').innerHTML = 'Loading backup..';
				view.querySelector('.backup_account').innerHTML = 'Loading backup..';
				view.querySelector('.backup_balance').innerHTML = 'Loading backup..';

				if(window.app.currentView == 'backup_restore')
				{
					view.querySelector('.new_account > .input').innerHTML = 'Backup account';
					view.querySelector('.old_account > .input').innerHTML = 'Current account';

					// Hide the options until we have filled in their information.
					view.querySelector('.old_account').style.display = "none";
					view.querySelector('.old_account').style.display = "none";
				}
			}

			// Create a backup wallet manager.
			window.app.backupManager = promisify(new bitcoreWallet({ baseUrl: NODE_BWS, verbose: true }));

			// Set it up to use the backup key.
			await window.app.backupManager.importFromExtendedPrivateKeyAsync(privateKey, { coin: 'bch' })

			// Check if the wallet is functional..
			if(window.app.backupManager.isComplete())
			{
				// Get the new balance.
				let balanceData = await window.app.backupManager.getBalanceAsync({});

				// Fetch the available value and exchange rate..
				let asset = balanceData.availableAmount / 100000000;
				let rate = window.app.currencyRates.filter(token => token.code === window.app.config.currency.current)[0].rate;

				// Calculate the current balance in the selected currency.
				backup.balance = (asset * rate);

				// Create a new address.
				let addressData = await window.app.backupManager.createAddressAsync({});

				// Locally store the CashAddr version of the address.
				backup.address = bitcoreWallet.BitcoreCash.Address(addressData.address).toCashAddress(true);

				// Look up the account on a public API.
				// TODO: Change this to use the bitcore node once this is resolved: https://github.com/bitpay/bitcore/pull/1881
				let lookupAccount = JSON.parse(await requestGet('https://rest.bitcoin.com/v2/rawtransactions/getRawTransaction/' + account.toString('hex')));
				let lookupRegistration = JSON.parse(await requestGet(NODE_NET + 'tx/' + account.toString('hex')));

				// Parse the registration transaction to get the account details.
				let finalAccount = await cashaccount.parseRegistration(Buffer.from(lookupAccount, 'hex'), Buffer.from(lookupRegistration.blockHash, 'hex'), false);

				// Go over each payload..
				for(let index in finalAccount.payloads)
				{
					// If this is a KeyHash or ScriptHash...
					if(finalAccount.payloads[index].type == 1 || finalAccount.payloads[index].type == 2)
					{
						// .. and it matches the backup wallets address ..
						if(finalAccount.payloads[index].address == ('bitcoincash:' + backup.address))
						{
							// TODO: properly handle collisions.
							backup.account = 
							{
								identifier: finalAccount.name + "#" + (lookupRegistration.blockHeight - cashaccount.blockModifier),
								name: finalAccount.name,
								number: (lookupRegistration.blockHeight - cashaccount.blockModifier)
							}
						}
					}
				}

				if(backup.account.identifier && backup.address && typeof backup.balance != 'undefined')
				{
					// Store the backup locally.
					window.app.backup = backup;

					if(window.app.currentView != 'home')
					{
						let view = document.querySelector('#' + window.app.currentView);

						// Show the balance, account and type.
						view.querySelector('.backup_type').innerHTML = 'Balance only';
						view.querySelector('.backup_account').innerHTML = backup.account.identifier;
						view.querySelector('.backup_balance').innerHTML = backup.balance.toLocaleString(Intl.DateTimeFormat().resolvedOptions().locale, { style: 'currency', currency: window.app.config.currency.current });

						if(window.app.currentView == 'backup_verify')
						{
							view.querySelector('#verify_backup').disabled = false;
							view.querySelector('#verify_backup').onclick = window.app.verifyBackup;
						}

						if(window.app.currentView == 'backup_restore')
						{
							// TODO: properly handle collisions.
							view.querySelector('.new_account').style.display = "flex";
							view.querySelector('.new_account > .input').innerHTML = backup.account.name + "&nbsp;<small>#" + backup.account.number + "</small>";

							if(typeof window.app.wallet !== 'undefined')
							{
								// Hide old account as it does not exist.
								view.querySelector('.old_account').style.display = "flex";
								view.querySelector('.old_account > .input').innerHTML = window.app.wallet.account.name + "&nbsp;<small>#" + window.app.wallet.account.number + "</small>";
							}
						}
					}
				}
				else
				{
					// Show the balance, account and type.
					view.querySelector('.backup_type').innerHTML = 'Broken';
					view.querySelector('.backup_account').innerHTML = 'Broken';
					view.querySelector('.backup_balance').innerHTML = 'Broken';
				}
			}
		}
		catch(error)
		{
			throwDebug('Failed to load backup: ' + JSON.stringify(error));
		}
	}

	async commitBackupAction()
	{
		try
		{
			// If we should restore a backup..
			if(window.app.backupAction === true)
			{
				// Notify that backup is started.
				window.plugins.toast.showShortCenter('Restoring backup.')
				
				// Send current funds to backup account, and store the transaction information
				let sweepStatus = await window.app.sweepWallet(window.app.walletManager, window.app.backup.address);

				// Change to the newly restored backup.
				await window.app.createWallet(window.app.backup.account.identifier, window.app.backup.secret.key, window.app.backup.account.identifier, false);

				// Update transactino history with transaction status.
				window.app.sweepToBackup(sweepStatus);

				// Reset backup action
				await window.app.selectBackupAction(null);
				
				// Discard backup information.
				delete window.app.backup;
				delete window.app.backupAction;
				delete window.app.backupManager;

				// Close backup view.
				await window.app.closeView();

				// If the user is on the welcome screen..
				if(window.app.currentView == 'welcome')
				{
					// Change to the home screen.
					await window.app.showView('home');
					await window.app.initializeView('home');
				}

				// Update transaction balance and history.
				await window.app.updateTransactionHistory(true, true);
			}
			else
			{
				// Notify that backup is started.
				window.plugins.toast.showShortCenter('Sweeping backup funds.')

				// Send backup funds to current account.
				await window.app.sweepFromBackup();

				// Reset backup action
				await window.app.selectBackupAction(null);
				
				// Discard backup information.
				delete window.app.backup;
				delete window.app.backupAction;
				delete window.app.backupManager;

				// Close backup view.
				window.app.closeView();

				// Update transaction balance and history.
				await window.app.updateTransactionHistory(true, true);
			}
		}
		catch(error)
		{
			throwDebug("Failed to commit backup action: " + JSON.stringify(error));
		}
	}

	async selectBackupAction(restore)
	{
		try
		{
			// Store backup action locally.
			window.app.backupAction = restore;

			//
			let view = document.querySelector('#' + window.app.currentView);

			if(restore === null)
			{
				view.querySelector('.new_account > .icon').className = 'icon fas fa-ban';
				view.querySelector('.old_account > .icon').className = 'icon fas fa-ban';
				
				view.querySelector('.new_account > .icon').style.color = 'black';
				view.querySelector('.old_account > .icon').style.color = 'black';

				view.querySelector('#apply_backup > span').innerHTML = "Choose action.";
				view.querySelector('#apply_backup').disabled = true;
				delete view.querySelector('#apply_backup').onclick;
			}

			if(restore === true)
			{
				view.querySelector('.new_account > .icon').className = 'icon fas fa-check';
				view.querySelector('.old_account > .icon').className = 'icon fas fa-times';

				view.querySelector('.new_account > .icon').style.color = 'green';
				view.querySelector('.old_account > .icon').style.color = 'red';

				view.querySelector('#apply_backup > span').innerHTML = "Restore backup account.";
				view.querySelector('#apply_backup').onclick = window.app.commitBackupAction;
				view.querySelector('#apply_backup').disabled = false;
			}

			if(restore === false)
			{
				view.querySelector('.new_account > .icon').className = 'icon fas fa-times';
				view.querySelector('.old_account > .icon').className = 'icon fas fa-check';

				view.querySelector('.new_account > .icon').style.color = 'red';
				view.querySelector('.old_account > .icon').style.color = 'green';

				view.querySelector('#apply_backup > span').innerHTML = "Sweep backup funds.";
				view.querySelector('#apply_backup').onclick = window.app.commitBackupAction;
				view.querySelector('#apply_backup').disabled = false;
			}
		}
		catch(error)
		{
			throwDebug('Failed to select backup action: ' + JSON.serialize(error));
		}
	}

	async initializeView(view = 'home')
	{
		try
		{
			// Set the initial view on the stack manually.
			window.app.viewStack = [ view ];
			window.app.currentView = view;
		}
		catch(error)
		{
			throwDebug('Failed to initialize a view', error);
		}
	}

	async setViewLabel(view, parentView)
	{
		try
		{
			// Find the view label string in the translations
			let stringName = "views." + parentView + ".title";
			let stringText = translations.strings.views[parentView].title[window.app.config.language.current];
			
			// Set a translation data type.
			document.getElementById(view).getElementsByClassName('toolbar')[0].getElementsByClassName('title')[0].setAttribute('data-string', stringName);
			
			// Set the label text.
			document.getElementById(view).getElementsByClassName('toolbar')[0].getElementsByClassName('title')[0].innerHTML = stringText;
		}
		catch(error)
		{
			// TODO: add missing view labels, then restore this.
			throwDebug('Failed to set view label (' + view + ', ' + parentView + ')', error);
		}
	}

	async parentView()
	{
		return window.app.viewStack[window.app.viewStack.length -1];
	}
	
	async showView(view, title = true)
	{
		try
		{
			// Hide the drawer if opened.
			document.getElementById('drawerControl').checked = false;

			// Push the current view to the stack.
			window.app.viewStack.push(window.app.currentView);

			// Hide the current view.
			document.getElementById(window.app.currentView).style.visibility = 'hidden';

			// Set the label is asked to.
			if(title)
			{
				window.app.setViewLabel(view, window.app.currentView);
			}

			// Start QR code scanner if necessary.
			if(view == 'interact' && typeof window.QRScanner !== 'undefined')
			{
				window.app.scanStart();
			}
			
			// Start QR code scanner if necessary.
			if(view == 'send')
			{
				window.app.showMaxValue();
			}

			// Set the new view as the current view.
			window.app.currentView = view;

			// Show the new (now current) view.
			document.getElementById(window.app.currentView).style.visibility = 'visible';
		}
		catch(error)
		{
			throwDebug('Failed to show viev', error);
		}
	}

	async resetForms(view)
	{
		try
		{
			// TODO: based on view, reset the forms..
			if(view == 'send')
			{
				// Reset currency to wallet currency.
				document.getElementById('send_amount').setAttribute('data-currency', window.app.config.currency.current);
				document.getElementById('send_currency').innerHTML = window.app.config.currency.current;
				
				// Remove the recipient and value.
				document.getElementById('recipientInput').value = '';
				document.getElementById('send_amount').value = '';
				
				// Trigger a changed recipient and value check.
				await window.app.changeRecipient();
				await window.app.changeSendValue();
			}

			if(view == 'receive')
			{
				// Reset currency to wallet currency.
				document.getElementById('receive_amount').setAttribute('data-currency', window.app.config.currency.current);
				document.getElementById('receive_currency').innerHTML = window.app.config.currency.current;
				
				// Remove the amount
				document.getElementById('receive_amount').value = '';

				// Clear any request expectations.
				window.app.receiveExpectationAmount = null;
				window.app.receiveExpectationSatoshis = null;
				window.app.receiveExpectationCurrency = null;
				
				// Reset the QR code the plain address.
				window.app.updateAddress();
				
				// TODO: Trigger a changed amount..
			}

			if(view == 'backup_restore')
			{
				await window.app.selectBackupAction(null);
			}

			if(view == 'backup_verify')
			{
				document.getElementById('verify_backup').disabled = true;
				delete document.getElementById('verify_backup').onclick;
			}
		}
		catch(error)
		{
			throwDebug('Failed to reset forms', error);
		}
	}

	async closeView()
	{
		try
		{
			// Hide the drawer if opened.
			document.getElementById('drawerControl').checked = false;

			// If there is more than one view in the stack..
			if(window.app.viewStack.length > 1)
			{
				// hide the current view and reset its scroll position.
				document.getElementById(window.app.currentView).style.visibility = 'hidden';
				document.getElementById(window.app.currentView).getElementsByClassName('content')[0].scrollTop = 0;

				// Reset any user entry into forms of the closed view.
				await window.app.resetForms(window.app.currentView);

				// Stop QR code scanner if necessary.
				if(window.app.currentView == 'interact' && typeof window.QRScanner !== 'undefined')
				{
					window.app.scanStop();
				}
				
				// Pop the top stack from the stack list.
				window.app.currentView = window.app.viewStack.pop();
				
				// Show the new view.
				document.getElementById(window.app.currentView).style.visibility = 'visible';
			}
			else
			{
				// Close the application.
				navigator.app.exitApp();
			}
		}
		catch(error)
		{
			throwDebug('Failed to close view', error);
		}
	}

	async changeReceiveValue()
	{
		try
		{
			// If there is an amount already stored...
			if(document.getElementById('receive_amount').hasAttribute('data-amount'))
			{
				// Remove it, it will get recalculated as needed.
				document.getElementById('receive_amount').removeAttribute('data-amount');
			}
			
			// Grab the currency and rate used for this send.
			let fiat = Number(document.getElementById('receive_amount').value);
			
			if(!isNaN(fiat))
			{
				let currency = document.getElementById('receive_amount').getAttribute('data-currency');
				let rate = window.app.currencyRates.filter(token => token.code === currency)[0].rate;
				
				// Calculate satoshis to send based on users fiat entry.
				let satoshis = Math.floor(fiat / rate * 100000000);
				
				if(satoshis > 0)
				{
					// Update the satoshi amount to send.
					document.getElementById('receive_amount').setAttribute('data-amount', satoshis);

					// Update the receive screen with the current address and payment requested amount.
					window.app.updateAddress(satoshis / 100000000);

					// Mark an expected receive amount and currency.
					window.app.receiveExpectationAmount = fiat;
					window.app.receiveExpectationSatoshis = satoshis;
					window.app.receiveExpectationCurrency = currency;
				}
				else
				{
					// Update the receive screen with the current address.
					window.app.updateAddress();
				}
			}
		}
		catch(error)
		{
			throwDebug('Failed to parse send value', error);
		}
	}

	async sendCash()
	{
		try
		{
			// Disable the send button to prevent accidental multiple sends.
			document.getElementById('send_button').disabled = true;
			
			// TODO: Validate data properly.
			// Read the send information.
			let send =
			{
				toName: document.getElementById('recipientInput').value,
				toAddress: document.getElementById('recipientInput').getAttribute('data-address'),
				toIconText: document.getElementById('recipientInput').getAttribute('data-icon-text'),
				toIconClass: document.getElementById('recipientInput').getAttribute('data-icon-class'),
				fiatCurrency: document.getElementById('send_amount').getAttribute('data-currency'),
				fiatAmount: document.getElementById('send_amount').value,
				satoshis: document.getElementById('send_amount').getAttribute('data-amount')
			}

			// If the targer address has the bitcoincash: prefix..
			if(send.toAddress.startsWith('bitcoincash:'))
			{
				// remove the prefix.
				send.toAddress = send.toAddress.substring(12);
			}

			// Check if we're sending to our own address..
			if(send.toAddress == window.app.wallet.address)
			{
				//TODO: Translate this.
				window.plugins.toast.showShortBottom('Cannot send to yourself.')

				// return false to break execution.
				return false;
			}

			// Configure transaction proposal options.
			let options = 
			{
				feePerKb: window.app.config.feeRatePerKB,
				outputs:
				[
					{
						toAddress: send.toAddress,
						amount: parseInt(send.satoshis)
					}
				]
			}
			
			try
			{
				// Create a proposal structure.
				let proposal = await window.app.walletManager.createTxProposalAsync(options);

				// Publish the proposal with the backend (part of the multisig workflow).
				let published = await window.app.walletManager.publishTxProposalAsync({ txp: proposal });

				// Sign the published proposal with our keys, which should complete the 1-of-1 process.
				let signed = await window.app.walletManager.signTxProposalAsync(published);

				// Broadcast the signed transaction by sending the signed proposal to the backend.
				let broadcast = await window.app.walletManager.broadcastTxProposalAsync(signed);

				// Vibrate.
				navigator.vibrate(VIBRATE['SUCCESS']);

				//TODO: Translate this.
				window.plugins.toast.showShortCenter('Sent')

				// Play the sending sound.
				if(window.app.config.sounds)
				{
					window.app.sounds.send.play();
				}
				
				// Store an outgoing entry in the wallet history.
				window.app.wallet.history.pending.unshift
				(
					{
						timestamp: moment().valueOf(),
						transaction: broadcast.txid,
						type: 'sent',
						icon: send.toIconClass,
						iconText: send.toIconText,
						text: send.toName,
						value: send.fiatAmount,
						currency: send.fiatCurrency
					}
				);

				// close the send screen.
				window.app.closeView();

				// Update the transaction history.
				await window.app.updateTransactionHistory(true);
			}
			catch (error)
			{
				if(error.message == "Amount below dust threshold")
				{
					//TODO: Translate this.
					window.plugins.toast.showShortBottom('Too small to send.')
				}
				else if(error.message == "Insufficient funds")
				{
					//TODO: Translate this.
					window.plugins.toast.showShortBottom('Not enough funds.')
				}
				else
				{
					throwDebug('Failed to send cash', error);
				}
			}
		}
		catch(error)
		{
			throwDebug('Failed to send cash', error);
		}
	}

	async changeSendValue()
	{
		try
		{
			// If there is an amount already stored...
			if(document.getElementById('send_amount').hasAttribute('data-amount'))
			{
				// Remove it, it will get recalculated as needed.
				document.getElementById('send_amount').removeAttribute('data-amount');
			}

			// Grab the currency and rate used for this send.
			let fiat = Number(document.getElementById('send_amount').value);

			if(!isNaN(fiat))
			{
				let currency = document.getElementById('send_amount').getAttribute('data-currency');
				let rate = window.app.currencyRates.filter(token => token.code === currency)[0].rate;

				// Calculate satoshis to send based on users fiat entry.
				let satoshis = Math.floor(fiat / rate * 100000000);

				if(satoshis > 0)
				{
					// Update the satoshi amount to send.
					document.getElementById('send_amount').setAttribute('data-amount', satoshis);
				}
			}

			// Update the send button based on user input.
			await window.app.updateSendButtonStatus();
		}
		catch(error)
		{
			throwDebug('Failed to parse send value', error);
		}
	}

	async showMaxValue()
	{
		try
		{
			// Initialize parameters needed to ask for max sendable amount.
			let options = 
			{
				feePerKb: window.app.config.feeRatePerKB,
				excludeUnconfirmedUtxos: false,
				returnInputs: false
			};
			
			// Ask the backend for a transaction structure for emptying the wallet.
			let maxTransaction = await window.app.walletManager.getSendMaxInfoAsync(options);
			
			// Calculate the fiat equivalent of the max amount of satoshis.
			let currency = document.getElementById('send_amount').getAttribute('data-currency');
			let rate = window.app.currencyRates.filter(token => token.code === currency)[0].rate;
			let maxFiat = 'maxfiat';
			
			// If this is a normal currency...
			if(currency !== 'BCH' && currency !== 'BTC')
			{
				// Show only 2 decimals.
				maxFiat = (maxTransaction.amount * rate / 100000000).toFixed(2);

				// Update the max display text.
				document.getElementById('max_funds').innerHTML = Number(maxFiat).toLocaleString(Intl.DateTimeFormat().resolvedOptions().locale, { style: 'currency', currency: currency });
			}
			else
			{
				// For bitcoin and bitcoin cash, show 8 decimals.
				maxFiat = (maxTransaction.amount * rate / 100000000).toFixed(8);

				// Update the max display text.
				document.getElementById('max_funds').innerHTML = currency + '&nbsp;' + maxFiat;
			}
		}
		catch(error)
		{
			throwDebug('Failed to show max sendable value');
		}
	}

	async selectMaxValue()
	{
		try
		{
			// Initialize parameters needed to ask for max sendable amount.
			let options = 
			{
				feePerKb: window.app.config.feeRatePerKB,
				excludeUnconfirmedUtxos: false,
				returnInputs: false
			};

			// Ask the backend for a transaction structure for emptying the wallet.
			let maxTransaction = await window.app.walletManager.getSendMaxInfoAsync(options);

			// Calculate the fiat equivalent of the max amount of satoshis.
			let currency = document.getElementById('send_amount').getAttribute('data-currency');
			let rate = window.app.currencyRates.filter(token => token.code === currency)[0].rate;
			let maxFiat = 'maxfiat';

			// If this is a normal currency...
			if(currency !== 'BCH' && currency !== 'BTC')
			{
				// Show only 2 decimals.
				maxFiat = (maxTransaction.amount * rate / 100000000).toFixed(2);
			}
			else
			{
				// For bitcoin and bitcoin cash, show 8 decimals.
				maxFiat = (maxTransaction.amount * rate / 100000000).toFixed(8);
			}

			// Update the amount entry with the satoshi amount and fiat presentation
			document.getElementById('send_amount').setAttribute('data-amount', maxTransaction.amount);
			document.getElementById('send_amount').value = maxFiat;

			//TODO: Translate this.
			window.plugins.toast.showShortBottom('Selected all funds.')
			
			// Update the button status.
			await window.app.updateSendButtonStatus();
		}
		catch(error)
		{
			throwDebug('Failed to select max send value', error);
		}
	}

	async selectRecipient(text)
	{
		try
		{
			// Update the input field with the selected recipient.
			document.getElementById('recipientInput').value = text;

			// Trigger a parsing of a changed recipient.
			await window.app.changeRecipient();
		}
		catch(error)
		{
			throwDebug('Failed to select send recipient', error);
		}
	}

	async changeRecipient()
	{
		try
		{
			// Change the icon to a spinner to indicate that we are parsing the input.
			document.getElementById('recipientIcon').className = 'icon fas fa-sync-alt loading';
			document.getElementById('recipientIcon').innerHTML = '';

			// Hide any address warning messages.
			document.getElementById('address_warning').display = 'none';

			// If there is an address already stored...
			if(document.getElementById('recipientInput').hasAttribute('data-address'))
			{
				// Remove it, it will get recalculated as needed.
				document.getElementById('recipientInput').removeAttribute('data-address');
			}

			// Initialize defaults for the interact icon for empty recipients.
			let iconClassName = 'icon --interact';
			let iconInnerHTML = '';

			// Store a local copy to avoid race conditions updating during parsing.
			let recipientText = document.getElementById('recipientInput').value;

			// If recipient string exists, assume it is invalid until proven otherwise.
			if(document.getElementById('recipientInput').value.length >= 1)
			{
				iconClassName = 'icon fas fa-ban';
				iconInnerHTML = '';
			}

			// Check if the entered string could be a valid cash account identifier.
			if(cashaccount.identifierRegexp.test(recipientText))
			{
				try
				{
					// Match the name to a parser to get the account parts.
					let accountParts = recipientText.match(cashaccount.identifierRegexp);

					// Name the parts for convenience.
					let name = accountParts[1];
					let number = accountParts[2];
					let hash = accountParts[4];

					// TODO: properly handle collisions.
					// Look up the account on a public API.
					let lookupAccount = JSON.parse(await requestGet('https://api.cashaccount.info/lookup/' + number + '/' + name));

					// Look up the block information from a public API
					let lookupBlock = JSON.parse(await requestGet(NODE_NET + '/block/' + (cashaccount.blockModifier + parseInt(number))));
					
					// Parse the registration transaction to get the account details.
					let finalAccount = await cashaccount.parseRegistration(Buffer.from(lookupAccount.results[0].transaction, 'hex'), Buffer.from(lookupBlock.hash, 'hex'), lookupAccount.results[0].inclusion_proof);

					// Go over each payload..
					for(let index in finalAccount.payloads)
					{
						// If this is a KeyHash or ScriptHash...
						if(finalAccount.payloads[index].type == 1 || finalAccount.payloads[index].type == 2)
						{
							// Store this as the address to use.
							document.getElementById('recipientInput').setAttribute('data-address', finalAccount.payloads[index].address);
						}
					}

					// If we found a usable payload to send to..
					if(document.getElementById('recipientInput').hasAttribute('data-address'))
					{
						// Set the icon to the account emoji to indicate that the entry is proper.
						iconClassName = 'icon';
						iconInnerHTML = finalAccount.emoji;
					}
				}
				catch(error)
				{
					// The recipient couldn't be resolved as a cash account identifier, moving on..
					console.log('failed to lookup possible cash account?');
				}
			}
			// TODO: Check if the entered string could be a valid cash address.
			else if(true)
			{
				try
				{
					let recipientAddress = recipientText;

					// If the scanned code is a bitcoin cash payment request..
					if(typeof recipientAddress == 'string' && recipientAddress.startsWith('bitcoincash:'))
					{
						// remove the prefix.
						recipientAddress = recipientAddress.substring(12);
					}

					let cashAddressInformation = false;

					// Try to parse the address.
					try
					{
						console.log(recipientAddress)

						// If the address is too short to be a cashaddr..
						if(recipientAddress.length < 35)
						{
							// Parse it as a legacy address.
							let legacy = bitcoreWallet.Bitcore.Address(recipientAddress);

							// Parse the legacy address with the BCH library.
							let cashaddr = bitcoreWallet.BitcoreCash.Address(legacy);

							// Convert the legacy address to a cash address string.
							recipientAddress = cashaddr.toCashAddress();

							// Display a warning.
							document.getElementById('address_warning').style.display = 'inline-block';
						}

						// Decode the address information.
						cashAddressInformation = bitcoreWallet.BitcoreCash.Address._decodeCashAddress(recipientAddress);
					}
					catch(error)
					{
						throw('Could not parse address: ' + error);
					}

					// Check if the address is a valid livenet address..
					if(cashAddressInformation.network.name == 'livenet')
					{
						// Store this as the address to use.
						document.getElementById('recipientInput').setAttribute('data-address', recipientAddress);

						// Set the icon to a QR code symbol.
						iconClassName = 'icon fas fa-qrcode';
						iconInnerHTML = '';
					}
				}
				catch(error)
				{
					// this was not a bitcoin cash address.
					console.log('Could not parse recipient string: ' + error);
				}
			}
			
			// Update icon presentation.
			document.getElementById('recipientIcon').className = iconClassName;
			document.getElementById('recipientIcon').innerHTML = iconInnerHTML;
			
			// Store this as the icon to use.
			document.getElementById('recipientInput').setAttribute('data-icon-class', iconClassName);
			document.getElementById('recipientInput').setAttribute('data-icon-text', iconInnerHTML);
			
			// Update the send button based on user input.
			await window.app.updateSendButtonStatus();
		}
		catch(error)
		{
			throwDebug('Failed to parse select recipient', error);
		}
	}

	async updateSendButtonStatus()
	{
		try
		{
			// Check if we have either an address and an amount..
			if(document.getElementById('recipientInput').hasAttribute('data-address') && document.getElementById('send_amount').hasAttribute('data-amount'))
			{
				// .. and enable the button
				document.getElementById('send_button').disabled = false;
			}
			else
			{
				// TODO: Verify that funds are enough.
				// TODO: Change send payment to verify payment if more than spend lock.

				// .. and disable the button.
				document.getElementById('send_button').disabled = true;
			}
		}
		catch(error)
		{
			throwDebug('Failed to update send button status', error);
		}
	}

	async updateCurrencyRates()
	{
		try
		{
			// Set the loading indicator to working state.
			document.getElementById('status_rates').className = 'loading row'
			document.getElementById('status_rates').childNodes[0].className = 'icon fas fa-sync-alt'

			// If we don't have any currency rates, or are missing the selected rate..
			if(!window.app.currencyRates || !window.app.currencyRates[window.app.config.currency.selected])
			{
				// Load the currency list, and when loaded store the currency data.
				await window.app.storeCurrencyData(await requestGet('https://bitpay.com/api/rates/BCH'));
			}
			// If we have currency rates from before..
			else
			{
				// .. and we're using a non-BCH currency..
				if(window.app.config.currency.current != 'BCH')
				{
					// Load the currency rate, and when loaded update the currency rate.
					await window.app.updateCurrencyData(await requestGet('https://bitpay.com/api/rates/BCH/' + window.app.config.currency.current));
				}
			}

			// Update presentation based on available currencies.
			await window.app.updateCurrencySelector();

			// Set the loading indicator to complete state.
			document.getElementById('status_rates').className = 'row'
			document.getElementById('status_rates').childNodes[0].className = 'icon fas fa-check'
		}
		catch(error)
		{
			throwDebug('Failed to update currency rates', error);
		}
	}

	//
	async updateCurrencyData(exchangeRates)
	{
		try
		{
			// Parse the currency data.
			let priceData = JSON.parse(exchangeRates);
			
			// Try to find the currency in the currency rate list.
			let currencyIndex = window.app.currencyRates.findIndex((currency => currency.code == priceData.code));

			// Update the currency rate information.
			window.app.currencyRates[currencyIndex].rate = priceData.rate;

			// Store the updated values.
			localStorage.currencyRates = JSON.stringify(window.app.currencyRates);
		}
		catch(error)
		{
			throwDebug('Failed to store currency data', error);
		}
	}

	//
	async storeCurrencyData(exchangeRates)
	{
		try
		{
			// Initialize empty currency lists.
			let currencyRates = [];
			
			// Parse the currency data.
			let priceData = JSON.parse(exchangeRates);
			
			// For each currency...
			for(let index in priceData)
			{
				// .. that is not Bitcoin BTC (which we will ignore)
				if(priceData[index].code != 'BTC')
				{
					// Populate names and rates.
					currencyRates.push({ code: priceData[index].code, rate: priceData[index].rate });
				}
			}

			// Overwrite any previously stored currency rates.
			window.app.currencyRates = currencyRates;
			
			// Store the current values.
			localStorage.currencyRates = JSON.stringify(window.app.currencyRates);
		}
		catch(error)
		{
			throwDebug('Failed to store currency data', error);
		}
	}

	compareObjectNames(a, b)
	{
		try
		{
			return (a.name > b.name ? 1 : ((b.name > a.name) ? -1 : 0));
		}
		catch(error)
		{
			return 0;
		}
	}
	
	compareObjectNamesInsensitive(a, b)
	{
		try
		{
			return (a.name.toLowerCase() > b.name.toLowerCase() ? 1 : ((b.name.toLowerCase() > a.name.toLowerCase()) ? -1 : 0));
		}
		catch(error)
		{
			return 0;
		}
	}
	
	compareTransactionTimestamps(a, b)
	{
		try
		{
			return (a.timestamp < b.timestamp ? 1 : ((b.timestamp < a.timestamp) ? -1 : 0));
		}
		catch(error)
		{
			return 0;
		}
	}

	compareObjectTransactions(a, b)
	{
		try
		{
			return (a.transaction > b.transaction ? 1 : ((b.transaction > a.transaction) ? -1 : 0));
		}
		catch(error)
		{
			return 0;
		}
	}
	
	compareCurrencyNames(a, b)
	{
		try
		{
			// If the name is missing..
			if(typeof window.app.currencyNames[window.app.config.language.current][a.code] === 'undefined')
			{
				// .. set it to an empty string.
				window.app.currencyNames[window.app.config.language.current][a.code] = '';
			}

			// If the name is missing..
			if(typeof window.app.currencyNames[window.app.config.language.current][b.code] === 'undefined')
			{
				// .. set it to an empty string.
				window.app.currencyNames[window.app.config.language.current][b.code] = '';
			}

			// Declare shorthands to the information to compare for legibility.
			const names = 
			{
				a: window.app.currencyNames[window.app.config.language.current][a.code].toLowerCase(),
				b: window.app.currencyNames[window.app.config.language.current][b.code].toLowerCase()
			}

			// Return the comparison compatible with the sort function.
			return (names.a > names.b ? 1 : ((names.b > names.a) ? -1 : 0));
		}
		catch(error)
		{
			console.log('Failed to compary currency names: ' + JSON.stringify(error));
			return 0;
		}
	}

	async updateCurrencyNames()
	{
		try
		{
			// Get a list of nodes that print out the language name or code.
			let nameNodes = document.getElementsByClassName('currencyName');
			let codeNodes = document.getElementsByClassName('currencyCode');

			// Update the display name.
			for(let index in nameNodes)
			{
				if(typeof nameNodes[index] == 'object')
				{
					nameNodes[index].innerHTML = window.app.currencyNames[window.app.config.language.current][window.app.config.currency.current];
				}
			}
			
			// Update the language code.
			for(let index in codeNodes)
			{
				if(typeof codeNodes[index] == 'object')
				{
					codeNodes[index].innerHTML = window.app.config.currency.current;
				}
			}

			// Update balance to show the new currency.
			window.app.updateWalletBalance();
		}
		catch(error)
		{
			throwDebug('Failed to update currency names', error);
		}
	}

	async updateCurrencySelector()
	{
		try
		{
			// Sanity check to make sure we have currency rates to work with.
			if(window.app.currencyRates)
			{
				// Sort currencies by name.
				window.app.currencyRates.sort(window.app.compareCurrencyNames);

				// Update the name of the relevant currencies based on the current language.
				//document.getElementById('currencyRegionalName').innerHTML = window.app.currencyNames[window.app.config.language.current][window.app.config.currency.regional];
				//document.getElementById('currencyDefaultName').innerHTML = window.app.currencyNames[window.app.config.language.current][window.app.config.currency.native];

				// Add the currencies to the currency selector.
				let relevantHTML = ''
				let relevantCurrencyList = document.getElementById('relevant_currencies');
				let currencyHTML = '';
				let currencyList = document.getElementById('currency_list');

				// For each currency that we have a valid exchange rate for...
				for(let index in window.app.currencyRates)
				{
					// .. check if we also have a name for it..
					if(window.app.currencyNames[window.app.config.language.current][window.app.currencyRates[index].code])
					{
						// Copy the code to make the code more legible.
						let currencyCode = window.app.currencyRates[index].code;

						if(typeof window.app.config.currency.relevant != 'undefined' && window.app.config.currency.relevant[currencyCode])
						{
							// Add this currency to the list of available currencies.
							relevantHTML += "<label class='row' onclick=\"app.setCurrency('" + currencyCode + "'); app.closeView();\"><span class='title capitalized'>" + window.app.currencyNames[window.app.config.language.current][currencyCode] + "</span><span class='icon'>" + currencyCode + "</span></label>";
						}

						// Add this currency to the list of available currencies.
						currencyHTML += "<label class='row' onclick=\"app.setCurrency('" + currencyCode + "'); app.closeView();\"><span class='title capitalized'>" + window.app.currencyNames[window.app.config.language.current][currencyCode] + "</span><span class='icon'>" + currencyCode + "</span></label>";
					}
				}

				// Overwrite the previous list of available currencies.
				currencyList.innerHTML = currencyHTML;
				relevantCurrencyList.innerHTML = relevantHTML;
			}
		}
		catch(error)
		{
			throwDebug('Failed to update currency selector', error);
		}
	}

	async display()
	{
		try
		{
			// Hide the loading screen.
			document.body.className = 'ready';
		}
		catch(error)
		{
			throwDebug('Failed to hide the splash screen', error);
		}
	}

	async shareAccount()
	{
		try
		{
			let identifier = window.app.wallet.account.emoji + ' ' + window.app.wallet.account.name + "#" + window.app.wallet.account.number;
			let url = 'https://api.cashaccount.info/display/' + window.app.wallet.account.name + "/" + window.app.wallet.account.number;
			let text = 'Send me cash with ' + identifier + ' on the #BitcoinCash network.\n\n' + url;

			navigator.share(text, 'Share payment information', 'text/plain');
		}
		catch(error)
		{
			throwDebug('Failed to share account information', error);
		}
	}
	
	async scanPrepare()
	{
		try
		{
			// Prepare the QR scanner for faster deployment, if previously authorized.
			window.QRScanner.getStatus(function(status) { if(status.authorized) { window.QRScanner.prepare(); } });
		}
		catch(error)
		{
			throwDebug('Failed to hide prepare camera for scanning', error);
		}
	}

	async shareError()
	{
		try
		{
			navigator.share(document.getElementById('errorMessage').getAttribute('data-error'), 'Share bug report', 'text/plain');
		}
		catch(error)
		{
			throwDebug('Failed to share bug report', error);
		}
	}

	async handleError(msg, url, line, col, error)
	{
		try
		{
			// Log the error stack to the console.
			console.trace();

			// Show the error view.
			await window.app.showView('error', false);

			// Reset the navigation view stack to prevent the user from exiting the error view.
			await window.app.initializeView('error');

			// Show the user interface, if not already shown.
			await window.app.display();

			// Store the error message so we can share it.
			document.getElementById('errorMessage').setAttribute('data-error', 'Cashual Wallet encountered an error: ' + msg + ", ```" + JSON.stringify(error, null, 4) + "```");

			// Fill in the error message.
			document.getElementById('errorMessage').innerHTML = msg;

			// Fill in the error content.
			document.getElementById('errorContent').innerHTML = JSON.stringify(error, null, 4);

			try
			{
				//
				let newIntervalNumber = setInterval(passthrough, 99999) + 1;

				while(newIntervalNumber--)
				{
					clearInterval(newIntervalNumber);
				}
			}
			catch(error)
			{
				console.log('Failed to clear interval in the error handler.');
			}
		}
		catch(error)
		{
			console.log('Failed to handle error');
			console.log('Giving up.');
		}
		
	}

	async startup()
	{
		try
		{
			// TODO: move function content to a promise.
			// If the app is running inside cordova..
			if(typeof cordova !== 'undefined')
			{
				// Set the back button to close any open views.
				document.addEventListener("backbutton", window.app.closeView.bind(window.app), false);
				
				// ...
				document.addEventListener("volumedownbutton", window.app.toggleTranslation.bind(window.app), false);
				document.addEventListener("volumeupbutton", window.app.toggleTheme.bind(window.app), false);

				// TODO: Evaluate if this really does keep the camera enabled and thus drains battery / invokes privacy concerns.
				window.app.scanPrepare();
			}

			// If this is the first startup of the app.
			if(typeof window.app.config == 'undefined')
			{
				// If the app is running inside cordova..
				if(typeof cordova !== 'undefined')
				{
					// .. get the users language and currency.
					cordova.plugins.cashual_wallet.getUserLocale(null, window.app.setupConfig.bind(window.app), alert);
				}
				else
				{
					// .. otherwise fake the users language and currency.
					window.app.setupConfig("{ \"languageCode\": \"sv\", \"languageName\": \"Svenska\", \"currencyCode\": \"SEK\", \"currencyName\": \"Swedish Krona\" }");
				}
			}

			//
			window.app.parsingConfiguration = window.app.parseConfig();
		}
		catch(error)
		{
			throwDebug('Failed to perfmorm application startup', error);
		}
	};

	async prepare()
	{
		// Check if we have local storage support..
		try
		{
			localStorage.setItem('supported', true);
			localStorage.removeItem('supported');
		}
		catch(error)
		{
			throw('Device does not support local storage. Shutting down to prevent loss of funds.');
		}

		try
		{

			// TODO: remove me later.
			if(typeof window.app.config != 'undefined' && typeof window.app.config.currency.relevant == 'undefined')
			{
				window.app.config.currency.relevant = 
				{
					BCH: true
				};
			}

			// Start loading locale names, before the device ready event.
			window.app.loadingLocaleNameData = window.app.loadLocaleNameData();

			// Start loading tranlations, before the device ready event.
			window.app.loadingTranslations = window.app.loadTranslations(translations.strings);

			// Finish initialization when cordova is ready.
			document.addEventListener('deviceready', window.app.startup.bind(window.app));

			// Start the process of getting updated currency rates.
			window.app.updatingCurrencyRates = window.app.updateCurrencyRates();

			// Set up the navigation view stack.
			await window.app.initializeView();

			// If we haven't created a wallet yet..
			if(typeof localStorage.wallet === 'undefined')
			{
				cordova.plugin.cloudsettings.exists
				(
					async function(exists)
					{
						if(exists)
						{
							// TODO: Show the backup restore screen.
							// await window.app.showView('welcome', false);

							// Load the cloud backup
							await window.app.loadCloudBackup();
						}
						else
						{
							// Show the welcome / first startup screen.
							await window.app.showView('welcome', false);
						}
					}
				);
			}

			// Load application sounds.
			window.app.sounds =
			{
				startup: new Audio('audio/startup.ogg'),
				receive: new Audio('audio/receive.ogg'),
				send: new Audio('audio/send.ogg'),
			};

			// Adjust volumes.
			window.app.sounds.startup.volume = 0.33;
			window.app.sounds.receive.volume = 0.77;
			window.app.sounds.send.volume = 0.55;
			
			//
			await window.app.setupWallet();

			// Enable NFC.
			nfc.enabled
			(
				function()
				{
					nfc.addNdefListener(window.app.onTouch, passthrough, alert);
				},
				passthrough
			);
		}
		catch(error)
		{
			// TODO: Update error message.
			throwDebug('Failed to prepare application startup', error);
		}
	}

	// Initialize...
	constructor()
	{
		// Create an instance of the bitcore wallet manager.
		this.walletManager = promisify(new bitcoreWallet({ baseUrl: NODE_BWS, verbose: true }));

		// For each locally stored piece of data..
		for(let index in localStorage)
		{
			// Check that it's a string (to avoid localstorage functions)
			if(typeof localStorage[index] === 'string' && index !== 'length' && index !== 'key')
			{
				//console.log('Parsing: ' + index + " from local storage.");

				// Load the data onto the main object.
				this[index] = JSON.parse(localStorage[index]);
			}
		}
	}
}

// Create a singleton instance of the wallet.
window.app = new CashualWallet();

// Set up a global error handler.
window.onerror = window.app.handleError;

// Start wallet preparations..
window.app.prepare();

// If this is in a browser...
if(!window.location.search)
{
	// Show the UI after 0.5 seconds.
	setTimeout(window.app.startup.bind(window.app), 500);
}
