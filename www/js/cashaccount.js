// Enable dependencies.
const BitcoreCash = require('bitcore-lib-cash');

module.exports =
{
	identifierHex: '01010101',
	blockModifier: 563620,
	nameRegexp: /[a-zA-Z0-9_]{1,99}/,
	hashRegexp: /[0-9]{1,10}/,
	identifierRegexp: /^([a-zA-Z0-9_]{1,99})#([0-9]{3,99})(\.([0-9]{1,10}))?$/,
	
	payloadTypes:
	{
		1: { name: 'Key Hash', length: 20 },
		2: { name: 'Script Hash', length: 20 },
		3: { name: 'Payment Code', length: 80 },
		4: { name: 'Stealth Keys', length: 66 }
	},
	
	emojiCodepoints: [ 128123, 128018, 128021, 128008, 128014, 128004, 128022, 128016, 128042, 128024, 128000, 128007, 128063, 129415, 128019, 128039, 129414, 129417, 128034, 128013, 128031, 128025, 128012, 129419, 128029, 128030, 128375, 127803, 127794, 127796, 127797, 127809, 127808, 127815, 127817, 127819, 127820, 127822, 127826, 127827, 129373, 129381, 129365, 127805, 127798, 127812, 129472, 129370, 129408, 127850, 127874, 127853, 127968, 128663, 128690, 9973, 9992, 128641, 128640, 8986, 9728, 11088, 127752, 9730, 127880, 127872, 9917, 9824, 9829, 9830, 9827, 128083, 128081, 127913, 128276, 127925, 127908, 127911, 127928, 127930, 129345, 128269, 128367, 128161, 128214, 9993, 128230, 9999, 128188, 128203, 9986, 128273, 128274, 128296, 128295, 9878, 9775, 128681, 128099, 127838 ],
	
	errors:
	{
		INVALID_NAME: 1,
		MISSING_PAYLOAD: 2,
		INVALID_PAYLOAD_LENGTH: 3
	},
	
	calculateAccountIdentity: function(blockhash, transactionHash)
	{
		// Step 1: Concatenate the block hash with the transaction hash
		let account_hash_step1 = Buffer.concat([blockhash, transactionHash]);
		
		// Step 2: Hash the results of the concatenation with sha256
		let account_hash_step2 = BitcoreCash.crypto.Hash.sha256(account_hash_step1);
		
		// Step 3: Take the first and last four bytes and discard the rest
		let account_hash_step3 = account_hash_step2.slice(0, 4);
		let account_emoji_step3 = account_hash_step2.slice(28, 32);
		
		// Step 4a: Convert to decimal notation and store as a string
		let account_hash_step4 = account_hash_step3.readUInt32BE(0).toString(10);
		
		// Step 4b: Select an emoji from the emojiHexList
		let emoji_index = account_emoji_step3.readUInt32BE(0) % 100;
		
		// Step 5: Reverse the the string so the last number is first
		let account_hash_step5 = account_hash_step4.toString().split("").reverse().join("").padEnd(10, '0');
		
		// Step 5b: calculate the integer codepoint for the emoji
		let emoji_codepoint = this.emojiCodepoints[emoji_index];
		
		// Return the final account identity.
		return { collisionHash: account_hash_step5, accountEmoji: emoji_codepoint };
	},
	
	parseAddress: function(payload_type, payload_data)
	{
		switch(payload_type)
		{
			// Type: Key Hash
			case 1:
			{
				return BitcoreCash.Address(payload_data, 'livenet', 'pubkeyhash').toCashAddress();
			}
			// Type: Script Hash
			case 2:
			{
				return BitcoreCash.Address(payload_data, 'livenet', 'scripthash').toCashAddress();
			}
			// Type: Payment Code
			case 3:
			{
				return BitcoreCash.encoding.Base58Check.encode(Buffer.concat([ Buffer.from('47', 'hex'), payload_data ]));
			}
			// Type: Stealth Keys
			case 4:
			{
				return null;
			}
		}
	},
	
	parseRegistration: async function(registration, blockhash, proof)
	{
		// Initialize an empty transaction object.
		let transaction = {};
		
		// 
		if(proof)
		{
			transaction.proof = Buffer.from(proof, 'hex');
		}

		try
		{
			// Parse the transaction into input and outputs.
			transaction.data = BitcoreCash.Transaction(registration);
			transaction.outputs = JSON.parse(JSON.stringify(transaction.data.outputs));
		
		
			// Calculate the transaction hash locally.
			transaction.transactionHashHex = transaction.data.hash;
			transaction.transactionHash = Buffer.from(transaction.data.hash, 'hex');
		}
		catch(error)
		{
			throw('Failed to parse registration: BitcoreCash was unabled to understand the transction.');
		}

		// Initialize control parameters to measure OP_RETURN outputs.
		let opReturnCount = 0;
		let opReturnIndex = null;
		
		// Go over each output in the transction and ..
		for(let outputIndex in transaction.outputs)
		{
			// Check if this output starts with an OP_RETURN opcode.
			if(transaction.outputs[outputIndex].script.startsWith('6a'))
			{
				// Increase the counter and store a link to this output.
				opReturnCount += 1;
				opReturnIndex = outputIndex;
			}
		}
		
		// 2a) Validate that there exist at least one OP_RETURN output.
		if(opReturnCount == 0)
		{
			console.log('Discarding [' + transaction.transactionHashHex + ']: Missing OP_RETURN output.');
			return false;
		}
		
		// 2b) Validating that there exist no more than a single OP_RETURN output.
		if(opReturnCount > 1)
		{
			console.log('Discarding [' + transaction.transactionHashHex + ']: Multiple OP_RETURN outputs.');
			return false;
		}
		
		// 3a) Validating that it has a protocol identifier
		if(!transaction.outputs[opReturnIndex].script.startsWith('6a0401010101'))
		{
			console.log('Discarding [' + transaction.transactionHashHex + ']: Invalid protocol identifier.');
			return false;
		}

		// Store the remainder of the transaction hex, after discarding the OP_RETURN and PROTOCOL IDENTIFIER push.
		let registrationHex = Buffer.from(transaction.outputs[opReturnIndex].script, 'hex').slice(6);
		let registrationParts = [];
		
		// While there is still data in the output..
		while(registrationHex.length > 1)
		{
			// Initialize default push and drop lengths.
			let pushLength = 0;
			let dropLength = 1;
			
			// Read the current opCode.
			let opCode = registrationHex.readUInt8(0);
			
			// Determine the length of the pushed data and control codes.
			if(opCode <= 75) { pushLength = opCode; }
			if(opCode == 76) { pushLength = registrationHex.readUInt8(1);    dropLength += 1; }
			if(opCode == 77) { pushLength = registrationHex.readUInt16BE(1); dropLength += 2; }
			if(opCode == 78) { pushLength = registrationHex.readUInt32BE(1); dropLength += 4; }
			
			// Remove the control codes.
			registrationHex = registrationHex.slice(dropLength);
			
			// Read and remove the pushlength if more than 0.
			if(pushLength > 0)
			{
				// Push the value to the parts array.
				registrationParts.push(registrationHex.slice(0, pushLength));
				
				// Remove the value data.
				registrationHex = registrationHex.slice(pushLength);
			}
		}

		// Initilalize an empty account object.
		let account = {};
		
		// Decode the account name and number.
		account.name = registrationParts[0].toString();
		//account.number = block.height - this.blockModifier;
		// TODO: Find the blockhash from the blockheight.
		// TODO: Validate the inclusion proof.
		if(blockhash)
		{
			try
			{
				let tempIdentity = this.calculateAccountIdentity(blockhash, transaction.transactionHash);

				account.emoji = String.fromCodePoint(tempIdentity.accountEmoji);
				account.hash = tempIdentity.collisionHash;
			}
			catch(error)
			{
				throw('Failed to calculate identity');
			}
		}
		
		// 3b) Validating that it has a valid account name.
		if(!this.nameRegexp.test(account.name))
		{
			console.log('Discarding [' + transaction.transactionHashHex + ']: Invalid account name.');
			return false;
		}
		
		// 4a) Validating that there exist at least one payload information
		if(registrationParts.length <= 1)
		{
			console.log('Discarding [' + transaction.transactionHashHex + ']: Missing payload information.');
			return false;
		}
		
		account.payloads = [];
		
		let payloadIndex = 0;
		while(++payloadIndex < registrationParts.length)
		{
			let payload =
			{
				type: registrationParts[payloadIndex].readUInt8(0),
				name: null,
				data: registrationParts[payloadIndex].slice(1),
				address: null
			};
			
			// If this is a known payload type..
			if(typeof this.payloadTypes[payload.type] !== 'undefined')
			{
				payload.name = this.payloadTypes[payload.type].name;
				
				// 4b) Validate length of known payload data.
				if(payload.data.length != this.payloadTypes[payload.type].length)
				{
					console.log('Ignoring [' + transaction.transactionHashHex + '] Payment [' + payloadIndex + ']: Invalid payload length.');
					return false;
				}

				try
				{
					// 4c) decode structure of known payload data.
					payload.address = this.parseAddress(payload.type, payload.data);
				}
				catch(error)
				{
					throw('Failed to parse address');
				}
			}
			
			// Store this payload in the account.
			account.payloads.push(payload);
		}
		
		//
		return account;
	}
}
