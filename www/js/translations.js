module.exports =
{
	strings:
	{
		// 
		views:
		{
			// The welcome screen is shown on first startup and lets you make a wallet.
			welcome:
			{
				title:
				{
					en: "New wallet",

					ca: "",
					cz: "",
					de: "",
					dk: "",
					es: "",
					fi: "",
					fr: "",
					jp: "",
					kh: "",
					lt: "",
					nl: "",
					no: "",
					ms: "",
					pl: "",
					pt: "Nova carteira",
					sv: "Ny plånbok",
					tr: "",
					vn: "",
					zh: "",
				},
				sectionSettingsTitle:
				{
					en: "Settings",

					ca: "",
					cz: "",
					de: "",
					dk: "",
					es: "",
					fi: "",
					fr: "",
					jp: "",
					kh: "",
					lt: "",
					nl: "",
					no: "",
					ms: "",
					pl: "",
					pt: "Opções",
					sv: "Inställningar",
					tr: "",
					vn: "",
					zh: "",
				},
				sectionSettingsText:
				{
					en: "Select your <b>language</b> and <b>currency</b>.",

					ca: "",
					cz: "",
					de: "",
					dk: "",
					es: "",
					fi: "",
					fr: "",
					jp: "",
					kh: "",
					lt: "",
					nl: "",
					no: "",
					ms: "",
					pl: "",
					pt: "Selecione o seu <b>idioma</b> e a sua <b>moeda</b>.",
					sv: "Välj ditt <b>språk</b> och <b>valuta</b>.",
					tr: "",
					vn: "",
					zh: "",
				},
				sectionNameTitle:
				{
					en: "Wallet",

					ca: "",
					cz: "",
					de: "",
					dk: "",
					es: "",
					fi: "",
					fr: "",
					jp: "",
					kh: "",
					lt: "",
					nl: "",
					no: "",
					ms: "",
					pl: "",
					pt: "Carteira",
					sv: "Plånbok",
					tr: "",
					vn: "",
					zh: "",
				},
				sectionNameText:
				{
					en: "Choose an <b>account name</b> that will be used as part of an identifier to receive money. <br /><br />To restore from a backup, tap the icon.",

					ca: "",
					cz: "",
					de: "",
					dk: "",
					es: "",
					fi: "",
					fr: "",
					jp: "",
					kh: "",
					lt: "",
					nl: "",
					no: "",
					ms: "",
					pl: "",
					pt: "Escolha um <b>nome de conta</b> que será a sua forma de identificação para as transferências monetárias. <br /><br />Para restaurar as definições guardadas, clique no símbolo.",
					sv: "Välj ett <b>kontonamn</b> som blir en del av dina kontouppgifter för att ta emot pengar.<br /><br />Tryck på ikonen för att återställa en backup.",
					tr: "",
					vn: "",
					zh: "",
				},
				sectionNameInput:
				{
					en: "Enter account name..",

					ca: "",
					cz: "",
					de: "",
					dk: "",
					es: "",
					fi: "",
					fr: "",
					jp: "",
					kh: "",
					lt: "",
					nl: "",
					no: "",
					ms: "",
					pl: "",
					pt: "Nome de conta..",
					sv: "Fyll i kontonamn..",
					tr: "",
					vn: "",
					zh: "",
				},
				buttonCreate:
				{
					en: "Create Wallet",

					ca: "",
					cz: "",
					de: "",
					dk: "",
					es: "",
					fi: "",
					fr: "",
					jp: "",
					kh: "",
					lt: "",
					nl: "",
					no: "",
					ms: "",
					pl: "",
					pt: "Criar carteira",
					sv: "Skapa Plånbok",
					tr: "",
					vn: "",
					zh: "",
				},
				buttonRestore:
				{
					en: "Restore Wallet",

					ca: "",
					cz: "",
					de: "",
					dk: "",
					es: "",
					fi: "",
					fr: "",
					jp: "",
					kh: "",
					lt: "",
					nl: "",
					no: "",
					ms: "",
					pl: "",
					pt: "Restaurar carteira",
					sv: "Återställ Plånbok",
					tr: "",
					vn: "",
					zh: "",
				},
			},

			// The home screen shows your balance and transaction history.
			home:
			{
				title:
				{
					en: "Wallet",
					
					ca: "",
					cz: "",
					de: "",
					dk: "",
					es: "",
					fi: "",
					fr: "",
					jp: "",
					kh: "",
					lt: "",
					nl: "",
					no: "",
					ms: "",
					pl: "",
					pt: "Carteira",
					sv: "Plånboken",
					tr: "",
					vn: "",
					zh: "",
				},
				pendingTitle:
				{
					en: "Pending",
					
					ca: "",
					cz: "",
					de: "",
					dk: "",
					es: "",
					fi: "",
					fr: "",
					jp: "",
					kh: "",
					lt: "",
					nl: "",
					no: "",
					ms: "",
					pl: "",
					pt: "Em espera",
					sv: "",
					tr: "",
					vn: "",
					zh: "",
				},
				todayTitle:
				{
					en: "Today",
					
					ca: "",
					cz: "",
					de: "",
					dk: "",
					es: "",
					fi: "",
					fr: "",
					jp: "",
					kh: "",
					lt: "",
					nl: "",
					no: "",
					ms: "",
					pl: "",
					pt: "Hoje",
					sv: "",
					tr: "",
					vn: "",
					zh: "",
				},
				// The transaction view show details about a transaction.
				transaction:
				{
					
				}
			},

			settings:
			{
				// The drawer screen lets the user navigate to other settings screens.
				drawer:
				{
					
				},

				// The language screen allows the user to change display language.
				language:
				{
					sectionDefaultTitle:
					{
						en: "Select language",

						ca: "",
						cz: "",
						de: "",
						dk: "",
						es: "",
						fi: "",
						fr: "",
						jp: "",
						kh: "",
						lt: "",
						nl: "",
						no: "",
						ms: "",
						pl: "",
						pt: "Escolher idioma",
						sv: "Välj språk",
						tr: "",
						vn: "",
						zh: "",
					},
					sectionDefaultText:
					{
						en: "Choose the <b>default language</b> or a language from the list of <b>translations</b> below.",

						ca: "",
						cz: "",
						de: "",
						dk: "",
						es: "",
						fi: "",
						fr: "",
						jp: "",
						kh: "",
						lt: "",
						nl: "",
						no: "",
						ms: "",
						pl: "",
						pt: "Escolha o <b>idioma predefinido</b> ou um dos outros idioma da lista de <b>traduções</b> apresentadas.",
						sv: "Välj <b>standard språket</b> eller ett språk från listan med <b>översättning</b> nedan.",
						tr: "",
						vn: "",
						zh: "",
					},
					sectionTranslationsTitle:
					{
						en: "Translations",

						ca: "",
						cz: "",
						de: "",
						dk: "",
						es: "",
						fi: "",
						fr: "",
						jp: "",
						kh: "",
						lt: "",
						nl: "",
						no: "",
						ms: "",
						pl: "",
						pt: "Traduções",
						sv: "Översättningar",
						tr: "",
						vn: "",
						zh: "",
					},
				},

				// The currency screen allows the user to change display currency.
				currency:
				{
					sectionDefaultTitle:
					{
						en: "Select currency",

						ca: "",
						cz: "",
						de: "",
						dk: "",
						es: "",
						fi: "",
						fr: "",
						jp: "",
						kh: "",
						lt: "",
						nl: "",
						no: "",
						ms: "",
						pl: "",
						pt: "Escolha a moeda",
						sv: "Välj valuta",
						tr: "",
						vn: "",
						zh: "",
					},
					sectionDefaultText:
					{
						en: "Choose one of these <b>relevant currencies</b> or below from the list of <b>available currencies</b>.",

						ca: "",
						cz: "",
						de: "",
						dk: "",
						es: "",
						fi: "",
						fr: "",
						jp: "",
						kh: "",
						lt: "",
						nl: "",
						no: "",
						ms: "",
						pl: "",
						pt: "Escolha uma destas <b>moedas mais populares</b> ou uma da seguinte lista de <b>moedas existentes</b>.",
						sv: "Välj en av dem <b>relevanta valutorna</b> eller nedan från listan med <b>tillgängliga valutor</b>.",
						tr: "",
						vn: "",
						zh: "",
					},
					sectionCurrenciesTitle:
					{
						en: "Available currencies",

						ca: "",
						cz: "",
						de: "",
						dk: "",
						es: "",
						fi: "",
						fr: "",
						jp: "",
						kh: "",
						lt: "",
						nl: "",
						no: "",
						ms: "",
						pl: "",
						pt: "Moedas existentes",
						sv: "Tillgängliga valutor",
						tr: "",
						vn: "",
						zh: "",
					},
				},

				// The spend lock screen allows the user to configure spend limits.
				lock:
				{
					
				},

				// The backup screens lets the user create, verify and restore backups.
				backup:
				{
					
				}
			},

			// The reminders screen informs the user about activities that needs doing.
			reminders:
			{
				title:
				{
					en: "Reminders",
					
					ca: "",
					cz: "",
					de: "",
					dk: "",
					es: "",
					fi: "",
					fr: "",
					jp: "",
					kh: "",
					lt: "",
					nl: "",
					no: "",
					ms: "",
					pl: "",
					pt: "Alertas",
					sv: "Påminnelser",
					tr: "",
					vn: "",
					zh: "",
				},
			},

			// The send view lets the user send money to a recipient.
			send:
			{
				title:
				{
					en: "Send",
					
					ca: "",
					cz: "",
					de: "",
					dk: "",
					es: "",
					fi: "",
					fr: "",
					jp: "",
					kh: "",
					lt: "",
					nl: "",
					no: "",
					ms: "",
					pl: "",
					pt: "Transferir",
					sv: "Skicka",
					tr: "",
					vn: "",
					zh: "",
				},
			},

			// The scan view lets the user interact with NFC, QR codes and similar.
			scan:
			{
				title:
				{
					en: "Scan",
					
					ca: "",
					cz: "",
					de: "",
					dk: "",
					es: "",
					fi: "",
					fr: "",
					jp: "",
					kh: "",
					lt: "",
					nl: "",
					no: "",
					ms: "",
					pl: "",
					pt: "Digitalizar",
					sv: "Skanna",
					tr: "",
					vn: "",
					zh: "",
				},
				sectionHelpTitle:
				{
					en: "Scan",
					
					ca: "",
					cz: "",
					de: "",
					dk: "",
					es: "",
					fi: "",
					fr: "",
					jp: "",
					kh: "",
					lt: "",
					nl: "",
					no: "",
					ms: "",
					pl: "",
					pt: "Digitalizar",
					sv: "Skanna",
					tr: "",
					vn: "",
					zh: "",
				},
				sectionHelpText:
				{
					en: "Focus the camera on a <b>code</b> according to the guide, or touch with a supported device.",
					
					ca: "",
					cz: "",
					de: "",
					dk: "",
					es: "",
					fi: "",
					fr: "",
					jp: "",
					kh: "",
					lt: "",
					nl: "",
					no: "",
					ms: "",
					pl: "",
					pt: "Centre a camera no <b>código QR</b> segundo as indicações, ou toque com um dispositivo compatível com NFC",
					sv: "Fokusera kameran på <b>koden</b> enligt mallen nedan, eller håll nära en kompatibel enhet.",
					tr: "",
					vn: "",
					zh: "",
				},
				sectionFileTitle:
				{
					en: "Open file",
					
					ca: "",
					cz: "",
					de: "",
					dk: "",
					es: "",
					fi: "",
					fr: "",
					jp: "",
					kh: "",
					lt: "",
					nl: "",
					no: "",
					ms: "",
					pl: "",
					pt: "Abrir ficheiro",
					sv: "Öppna fil",
					tr: "",
					vn: "",
					zh: "",
				},
			},

			// The receive screen lets the user share payment data or request payments.
			receive:
			{
				title:
				{
					en: "Receive",
					
					ca: "",
					cz: "",
					de: "",
					dk: "",
					es: "",
					fi: "",
					fr: "",
					jp: "",
					kh: "",
					lt: "",
					nl: "",
					no: "",
					ms: "",
					pl: "",
					pt: "A receber",
					sv: "Ta emot",
					tr: "",
					vn: "",
					zh: "",
				},
			},
		},

		// ...
		names:
		{
			theme:
			{
				day:
				{
					en: "Day mode",
					
					ca: "",
					cz: "",
					de: "",
					dk: "",
					es: "",
					fi: "",
					fr: "",
					jp: "",
					kh: "",
					lt: "",
					nl: "",
					no: "",
					ms: "",
					pl: "",
					pt: "Modo claro",
					sv: "Dagläge",
					tr: "",
					vn: "",
					zh: "",
				},
				night:
				{
					en: "Night mode",
					
					ca: "",
					cz: "",
					de: "",
					dk: "",
					es: "",
					fi: "",
					fr: "",
					jp: "",
					kh: "",
					lt: "",
					nl: "",
					no: "",
					ms: "",
					pl: "",
					pt: "Modo escuro",
					sv: "Nattläge",
					tr: "",
					vn: "",
					zh: "",
				}
			},
			sounds:
			{
				enabled:
				{
					en: "Play sound effects",
					
					ca: "",
					cz: "",
					de: "",
					dk: "",
					es: "",
					fi: "",
					fr: "",
					jp: "",
					kh: "",
					lt: "",
					nl: "",
					no: "",
					ms: "",
					pl: "",
					pt: "Efeitos sonoros activados",
					sv: "Spela ljudeffekter",
					tr: "",
					vn: "",
					zh: "",
				},
				disabled:
				{
					en: "No sound effects",
					
					ca: "",
					cz: "",
					de: "",
					dk: "",
					es: "",
					fi: "",
					fr: "",
					jp: "",
					kh: "",
					lt: "",
					nl: "",
					no: "",
					ms: "",
					pl: "",
					pt: "Efeitos sonoros desativados",
					sv: "Inga ljudeffekter",
					tr: "",
					vn: "",
					zh: "",
				}
			}
		},
	}
}
