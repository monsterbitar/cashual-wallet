module.exports =
{
	// Debug
	debug: require('debug')('cashual:debug'),
	
	// Notice
	notice: require('debug')('cashual:notice'),
	
	// Warning
	warning: require('debug')('cashual:warning'),
	
	// Error
	error: require('debug')('cashual:error'),
	
	// Fatal
	fatal: require('debug')('cashual:fatal'),
}
