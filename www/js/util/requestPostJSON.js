module.exports = function(url, parameters)
{
	return new Promise
	(
		function(resolve, reject)
		{
			// Create a new HTTP request object.
			let xmlhttp = new XMLHttpRequest();
			
			// Prepare to send the request.
			xmlhttp.open('POST', url);
			
			// Assign handler for the response data.
			xmlhttp.onload = function()
			{
				if(this.status >= 200 && this.status < 300)
				{
					resolve(xmlhttp.response);
				}
				else
				{
					reject({ status: this.status, statusText: xmlhttp.statusText });
				}
			};
			
			// Assign handler for errors.
			xmlhttp.onerror = function()
			{
				reject({ status: this.status, statusText: xmlhttp.statusText });
			};
			
			// Set the content type.
			xmlhttp.setRequestHeader("Content-type", "application/json; charset=utf-8");
			
			// Send the request.
			xmlhttp.send(JSON.stringify(parameters));
		}
	);
}
