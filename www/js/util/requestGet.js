module.exports = function(url)
{
	return new Promise
	(
		function(resolve, reject)
		{
			// Create a new HTTP request object.
			let xmlhttp = new XMLHttpRequest();
			
			// Prepare to send the request.
			xmlhttp.open('GET', url);
			
			// Assign handler for the response data.
			xmlhttp.onload = function()
			{
				if(this.status >= 200 && this.status < 300)
				{
					resolve(xmlhttp.response);
				}
				else
				{
					reject({ status: this.status, statusText: xmlhttp.statusText });
				}
			};
			
			// Assign handler for errors.
			xmlhttp.onerror = function()
			{
				reject({ status: this.status, statusText: xmlhttp.statusText });
			};
			
			// Send the request.
			xmlhttp.send();
		}
	);
}

