module.exports =
{
	config:
	{
		language:
		{
			current: 'en',
			regional: 'en',
			selected: 'en',
			native: 'en',
		},
		currency:
		{
			current: 'BCH',
			regional: 'BCH',
			native: 'BCH',
			relevant:
			{
				'BCH': true,
			},
		},
		backup:
		{
			cloud: true,
			encryption: false,
			overwrite: false
		},
		feeRatePerKB: 1111,
		theme: 'day',
		sounds: true
	},
	wallet:
	{
		birth: null,
		address: null,
		account:
		{
			name: null,
			number: null,
			emoji: null,
			hash: null,
			identifier: null
		},
		history:
		{
			range:
			{
				from: null,
				to: null
			},
			pending: [],
			entries: [],
			transactions: {}
		},
		secret: null
	}
}
